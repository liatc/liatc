const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/activeFlights/app.js', 'public/js/active.js')
    .js('resources/js/strip/app.js', 'public/js/strip.js')
    .js('resources/js/activeATC/liveatc.js', 'public/js/liveatc.js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/strips.scss', 'public/css/strips.css')
    .copy('resources/img', 'public/img')
    .copy('resources/sound', 'public/sound');
