<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flights', function (Blueprint $table) {
            $table->foreign('airline_id')->references('id')->on('virtual_airlines')->cascadeOnDelete();
            $table->bigInteger('airline_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flights', function (Blueprint $table) {
            $table->bigInteger('airline_id')->unsigned()->change();
            $table->dropForeign('virtual_airlines_id_foreign');
        });
    }
}
