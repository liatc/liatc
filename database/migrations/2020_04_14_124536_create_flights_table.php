<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('airline_id')->unsigned()->nullable();
            $table->string('callsign');
            $table->bigInteger('pilot_id')->unsigned();
            $table->dateTime('departure_time');
            $table->string('aircraft_type');
            $table->string('origin');
            $table->string('destination');
            $table->string('alternative')->nullable();
            $table->integer('altitude');
            $table->integer('true_airspeed');
            $table->longText('routing');
            $table->longText('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
