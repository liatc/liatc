<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScratchpadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scratchpads', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('community_id')->unsigned();
            $table->longText('contents')->nullable();
            $table->bigInteger('owner_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('community_id')->references('id')->on('communities')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scratchpads');
    }
}
