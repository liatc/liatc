<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscordSyncTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discord_sync', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('community_id')->unsigned();
            $table->bigInteger('role_id')->unsigned();
            $table->bigInteger('va_id')->unsigned()->nullable();
            $table->string('permission')->nullable();
            $table->timestamps();

            $table->foreign('va_id')->references('id')->on('virtual_airlines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discord_sync');
    }
}
