<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripsToFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flights', function (Blueprint $table) {
            $table->string('beacon_code')->nullable();
            $table->integer('assigned_altitude')->nullable();
            $table->longText('notes')->nullable();
            $table->boolean('ifr')->default(true);
            $table->string('pending_handoff')->nullable();
            $table->integer('revision')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flights', function (Blueprint $table) {
            $table->dropColumn('beacon_code');
            $table->dropColumn('assigned_altitude');
            $table->dropColumn('notes');
            $table->dropColumn('ifr');
            $table->dropColumn('pending_handoff');
            $table->dropColumn('revision');
        });
    }
}
