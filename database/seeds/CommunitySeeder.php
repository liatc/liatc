<?php

use Illuminate\Database\Seeder;

class CommunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f = Faker\Factory::create();

        for($i=0;$i<10;$i++) {
            \App\Community::create(['name' => $f->company, 'plan_id' => rand(0, 1), 'owner_id' => 1, 'subdomain' => $f->word]);
        }
    }
}
