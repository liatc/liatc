import Pusher from 'pusher-js';
import $ from 'jquery';
import Vue from 'vue';
import ActiveApp from "./components/ActiveApp";
import Flight from "./components/Flight";

let globals = require('./globals');

globals.apiToken = $("meta[name=X-API-KEY]").attr('content');
globals.communityId = $("meta[name=X-COMMUNITY-ID]").attr('content');

globals.pusher = new Pusher(process.env.MIX_PUSHER_APP_KEY, {
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    forceTLS: true,
    authEndpoint: '/broadcasting/auth'
});

globals.pusherChannel = globals.pusher.subscribe('presence-c.' + globals.communityId + ".flights");
globals.pusherChannel.bind('pusher:subscription_succeeded', () => {
    loadVue();
});

function loadVue() {
    Vue.component('activeapp', ActiveApp);
    Vue.component('Flight', Flight);
    globals.app = new Vue({
        el: '#vue-app'
    });
}


