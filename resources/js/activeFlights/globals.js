let globals = {
    pusher: null,
    pusherChannel: null,
    app: null,
    apiToken: null,
    communityId: null
}

module.exports = globals;
