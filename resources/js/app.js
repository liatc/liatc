import $ from 'jquery';

window.$ = $;

$(".navbar-burger").click(() => {
    $(".navbar-burger").toggleClass('is-active');
    $("#navbar").toggleClass('is-active');
});

$(".modalbutton").click((e) => {
    let f = $(e.target);
    $(f.attr("data-target")).addClass("is-active");
});

$(".modal-close").click(e => {
    let f = $(e.target);
    f.parent().removeClass("is-active");
});
