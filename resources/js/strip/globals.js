let globals = {
    app: null,
    pusher: null,
    pusher_channel: null,
    message_channel: null,
    echo: null,
    user: null,
    api_key: null,
    community_id: null,
    sounds: {
        handoff: null,
        handoffAccept: null,
        message: null
    },
    shouldShowStrip(targetStation, yourStation, online) {
        if(targetStation == null) return false;
        if(targetStation.split("_").length == 1) return false;
        if(yourStation.split("_").length == 1) return false;
        let target = globals.decodeStation(targetStation);
        let your = globals.decodeStation(yourStation);
        if(your.station === "SUP" || your.station === "OBS") {
            // Supervisor override
            if(your.ap === "XSA") return true;
            if(your.ap == target.ap) return true;
        }

        if(target.ap != your.ap && your.ap != "XSA") return false;
        if(target.order > your.order) return false;

        let show = true;
        online.forEach(e => {
            e = e.station_name;
            if(e.split("_").length == 1) return;
            let dc = globals.decodeStation(e);
            if(e != yourStation && target.ap == dc.ap  && dc.order != your.order)
            {
                if(dc.order < your.order && target.order <= dc.order) {
                    show = false;
                }
            } else if(e != yourStation && target.ap == dc.ap && dc.order == your.order) {
                if(target.uq != null && target.uq == dc.uq) show = false;
            }
        });
        return show;

    },
    STATION_ORDER: {
        DEL: 0,
        GND: 1,
        TWR: 2,
        DEP: 3,
        APP: 3,
        CTR: 4,
        OCE: 4
    },
    decodeStation(name) {
        let spl = name.split('_');
        let inf =  {ap: spl[0], station: spl.length == 2 ? spl[1] : spl[2], uq: spl.length == 2 ? null : spl[1]};
        inf.order = globals.STATION_ORDER[inf.station];
        return inf;
    }
};

export default globals;
