import Vue from 'vue';
import Pusher from 'pusher-js';
import StripsApp from "./components/StripsApp";
import FlightStrip from "./components/FlightStrip";
import ControllerList from "./components/ControllerList";
import PendingHandoff from "./components/PendingHandoff";
import StripLookup from "./components/StripLookup";
import Scratchpad from "./components/Scratchpad";
import ChartView from "./components/ChartView";
import VFRGen from "./components/VFRGen";
import Chat from "./components/Chat";
import $ from "jquery";

let globals = require('./globals').default;
window.jQuery = require('jquery');
require('../../semantic/semantic');

globals.sounds.handoff = new Audio('/sound/tone_s.mp3');
globals.sounds.handoffAccept = new Audio('/sound/tone_t.mp3');
globals.sounds.message = new Audio('/sound/message.mp3');

globals.api_key = jQuery("meta[name=X-API-KEY]").attr('content');
globals.community_id = $("meta[name=X-COMMUNITY-ID]").attr('content');
fetch('/api/user?api_token=' + encodeURIComponent(globals.api_key)).then(e => e.json()).then(e => {
    globals.user = e;
    pusherInit();
});

function pusherInit() {
    Pusher.logToConsole = process.env.node_env !== "PRODUCTION";
    globals.pusher = new Pusher(process.env.MIX_PUSHER_APP_KEY, {
        cluster: process.env.MIX_PUSHER_APP_CLUSTER,
        authEndpoint: '/broadcasting/auth',
        forceTLS: true
    });
    globals.message_channel = globals.pusher.subscribe('private-c.' + globals.community_id + ".chat");
    globals.pusher_channel = globals.pusher.subscribe('presence-c.' + globals.community_id + ".flights");
    globals.pusher_channel.bind('pusher:subscription_succeeded', e => {
        vueInit();
    });
}



function vueInit() {
    console.log("Vue Init");
    Vue.component('stripsapp', StripsApp);
    Vue.component('FlightStrip', FlightStrip);
    Vue.component('ControllerList', ControllerList);
    Vue.component('PendingHandoff', PendingHandoff);
    Vue.component('StripLookup', StripLookup);
    Vue.component('Scratchpad', Scratchpad);
    Vue.component('ChartView', ChartView);
    Vue.component("VFRGen", VFRGen);
    Vue.component("Chat", Chat);

    $("#loading").hide();

    globals.app = new Vue({
        el: "#app"
    });
}

window.globals = globals;
