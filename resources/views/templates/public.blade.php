@extends('templates.app')

@section('body')
    <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ route('home') }}">
                LithiumATC
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbar">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbar" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="{{ route('home') }}">
                    Home
                </a>

                <a href="{{ route('pricing') }}" class="navbar-item">
                    Pricing
                </a>
            </div>

            <div class="navbar-end">
                @if(Auth::check() && \Illuminate\Support\Facades\Auth::user()->admin)
                    <a class="navbar-item" href="{{ route('admin.index') }}">
                        Administration
                    </a>
                @endif
                <div class="navbar-item">
                    <div class="buttons">
                        @auth
                            <button class="button is-light" onclick="document.getElementById('logout').submit()">
                                Logout
                            </button>
                            <form style="display: none" id="logout" method="POST" action="{{ route('logout') }}">@csrf</form>
                        @else
                            <a class="button is-light" href="{{ route('login') }}">
                                Log in
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </nav>
    @include('components.alertbanner')
    @yield('pub_body')
@endsection
