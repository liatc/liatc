@extends('templates.community', ['title'=>community()->name . ' | LithiumATC'])

@section('header')
    <style>
        html {
            height: 100%;
        }

        body {
            {{ \App\Community::resolveColor(community()->customization['backgrounds-atc'], true) }}
            min-height: 100%;
        }
    </style>
    @yield('atc_header')
@endsection

@section('pub_body')
    <div class="container mt-3 mb-3 slide-in-fwd-center">
        <div class="columns">
            <div class="column is-one-fifth">
                <aside class="menu" id="livelist">
                    <p class="menu-label">ATC | {{ community()->name }}</p>
                    <ul class="menu-list">
                        <li><a href="{{ coute('c.atc.index') }}" class="{{ active('c.atc.index') }}">Overview</a></li>
                        <li><a href="{{ coute('c.atc.active') }}" class="{{ active('c.atc.active') }}">Current Active Flights</a></li>
                        @if(community()->getPlan()->getCanUseFlightStrips())
                        <!-- check if their plan supports the bloody flight strips! -->
                        <li><a href="{{ coute('c.atc.strips') }}" class="{{ active('c.atc.strips') }}">Flight Strips</a></li>
                        @endif
                        <!-- okay u can go on about ur day now :) -->
                        <li><a href="{{ coute('c.atc.frequency') }}" class="{{ active('c.atc.frequency') }}">Change Frequencies</a></li>
                    </ul>
                    <p class="menu-label">ATC Online:</p>
                    <ul class="menu-list">
                        <controllerlist />
                    </ul>
                </aside>
            </div>
            <div class="column">
                @yield('atc_body')
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('/js/liveatc.js') }}"></script>
    @yield('atc_footer')
@endsection
