@extends('templates.community')

@section('header')
    <style>
        body{
        {{ \App\Community::resolveColor(community()->customization['backgrounds-admin'], true) }}
}
    </style>
@endsection

@section('pub_body')
    <div class="container mt-3 pb-3">
        <div class="columns">
            <div class="column is-one-fifth">
                <aside class="menu">
                    <p class="menu-label">General Management</p>
                    <ul class="menu-list">
                        <li><a href="{{ coute('c.admin.index') }}" class="{{ active('c.admin.index') }}">General Settings</a></li>
                        @if(community()->getPlan()->getCanCustomize())
                        <li><a href="{{ coute('c.admin.settings') }}" class="{{ active('c.admin.settings') }}">Extended Settings</a></li>
                        @endif
                        <li><a href="{{ coute('c.admin.airlines.index') }}" class="{{ active('c.admin.airlines') }}">Virtual Airlines</a></li>
                        <li><a href="{{ coute('c.admin.airports.index') }}" class="{{ active('c.admin.airports') }}">Airports</a></li>
                    </ul>
                    <p class="menu-label">User Control</p>
                    <ul class="menu-list">
                        <li><a href="{{ coute('c.admin.discord.index') }}" class="{{ active('c.admin.discord') }}">Discord Sync</a></li>
                    </ul>
                </aside>
            </div>
            <div class="column">
                @yield('admin_body')
            </div>
        </div>
    </div>
@endsection
