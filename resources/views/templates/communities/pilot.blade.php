@extends('templates.community', ['title'=>community()->name . ' | LithiumATC'])

@section('header')
    <style>
        html{
            height: 100%;
        }
        body{
            {{ \App\Community::resolveColor(community()->customization['backgrounds-pilot'], true) }}
            min-height: 100%;
        }
    </style>
@endsection

@section('pub_body')
    <div class="container mt-3 pb-3 slide-in-fwd-center">
        <div class="columns">
            <div class="column is-one-fifth">
                <aside class="menu">
                    <p class="menu-label">My Dashboard | {{ community()->name }}</p>
                    <ul class="menu-list">
                        <li><a href="{{ coute('c.pilotdash') }}" class="{{ active('c.pilotdash') }}">Overview</a></li>
                        <li><a href="{{ coute('c.fileplan') }}" class="{{ active('c.fileplan') }}">File New Flight Plan</a></li>
                        <li><a href="{{ coute('c.pilot.charts') }}" class="{{ active('c.pilot.charts') }}">Charts</a></li>
                        {{--<li><a href="#">Visit My Virtual Airline</a></li>--}}
                    </ul>
                    <p class="menu-label">ATC Online:</p>
                    <ul class="menu-list">
                        @php
                            $cc = community()->controllerPresence;
                        @endphp
                        @foreach($cc as $c)
                            <li><a>{{ $c->user->station_name == null ? $c->user->name : $c->user->station_name }}: {{ $c->user->frequency }}</a></li>
                        @endforeach
                        @if(count($cc) == 0)
                            <li><a>No online controllers</a></li>
                        @endif
                    </ul>
                </aside>
            </div>
            <div class="column">
                @yield('pilot_body')
            </div>
        </div>
    </div>
@endsection
