@extends('templates.public')


@section('header')
    <style>
        html{
            height: 100%;
        }
        body{
            background: linear-gradient(125deg, #ECFCFF 0%, #ECFCFF 40%, #B2FCFF calc(40% + 1px), #B2FCFF 60%, #5EDFFF calc(60% + 1px), #5EDFFF 72%, #3E64FF calc(72% + 1px), #3E64FF 100%);
            height: 100%;
        }
    </style>
@endsection

@section('pub_body')
    <div class="container mt-3 mb-3 slide-in-fwd-center">
        <div class="columns">
            <div class="column is-one-fifth">
                <aside class="menu">
                    <p class="menu-label">Administration Portal</p>
                    <ul class="menu-list">
                        <li><a href="{{ route('admin.index') }}" class="{{ active('admin.index') }}">Overview</a></li>
                        <li><a href="{{ route('admin.communitie') }}" class="{{ active('admin.communitie') }}">Communities</a></li>
                        <li><a href="{{ route('admin.settings') }}" class="{{ active('admin.settings') }}">System Settings</a></li>
                    </ul>
                </aside>
            </div>
            <div class="column">
                @yield('admin_body')
            </div>
        </div>
    </div>
@endsection
