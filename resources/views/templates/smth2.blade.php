@extends('templates.app')

@section('body')
    <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ route('home') }}">
                {{ community()->name }} - LithiumATC
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbar">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbar" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Home
                </a>

                @perm('admin')
                <a href="{{ coute('c.admin') }}" class="navbar-item">
                    Admin
                </a>
                @endperm
            </div>
        </div>
    </nav>
    @yield('pub_body')
@endsection
