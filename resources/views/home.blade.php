@extends('templates.public')

@section('header')
<style>
body{
    background: linear-gradient(45deg, #000850 0%, #000320 100%), radial-gradient(100% 225% at 100% 0%, #FF6928 0%, #000000 100%), linear-gradient(225deg, #FF7A00 0%, #000000 100%), linear-gradient(135deg, #CDFFEB 10%, #CDFFEB 35%, #009F9D 35%, #009F9D 60%, #07456F 60%, #07456F 67%, #0F0A3C 67%, #0F0A3C 100%);
background-blend-mode: screen, overlay, hard-light, normal;
}
</style>
@endsection
@section('pub_body')
<section class="section">
<div class="container">
@auth
<div class="notification is-warning">
<p class="has-text-dark">
    <strong>Notice:</strong> You are currently signed in! You will need to select your community to access their version of LithiumATC.
</p>
</div>
@else

@endauth
</div>
</section>
  <section class="section">
    <div class="container">
      <h1 class="title has-text-white">
        LithiumATC
      </h1>
      <p class="subtitle has-text-white">
        Advanced Air Traffic Control software for GTA V.<br>
        Powered by the <a href="https://www.pontifex.tech" class="logogreen">Pontifex Cloud Platform</a>.
      </p>
      <br>
      @auth
      <a data-target="#modal" class="button is-link modalbutton">
          <i class="fas fa-link"></i>&nbsp;Select Community
      </a>
      @else
      <a href="{{ route('login') }}" class="button is-link modalbutton">
          <i class="fab fa-discord"></i>&nbsp;Start using today
      </a>
      @endauth
    </div>
    </section>
    <section class="section">
    <div class="container">

    </div>
    <br>
    <div class="container">

    </div>
  </section>
  <section class="section">

  </section>
  <section class="section">
      <div class="container">
          <p class="subtitle has-text-white">
              &copy;2020 <a href="https://tobyroyal.codes/" class="logogreen">Toby Royal</a> & <a href="https://devjoe.net">Joe Longendyke</a> in cooperation with <a href="https://www.pontifex.tech/" class="logogreen">Pontifex Technologies</a> - All Rights Reserved.<br>
          </p>
      </div>
  </section>
    <section class="section">
      <br><br><br><br><br>
  </section>


  <div id="modal" class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <section class="section">
        <div class="container">
            <p class="subtitle has-text-white">
            Select your community
            </p>
            <form method="GET" action="{{ route('communityredir') }}">
                <div class="select">
                    <select name="cm">
                        <option value="-1" disabled selected>Select Community</option>
                        @foreach($communities as $c)
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div><br><br>
                <button class="button is-link">Go to community</button>
            </form>

        </div>
    </section>
  </div>
  <button class="modal-close is-large" aria-label="close"></button>
</div>

@endsection
@section('footer')
<script defer src="https://greenprint.tobyroyal.codes/js/all.js"></script>
<script>

</script>
@endsection
