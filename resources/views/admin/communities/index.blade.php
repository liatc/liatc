@extends('templates.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">Community Listing *oooo*</h1>
        <a href="{{ route('admin.communities.create') }}" class="button is-link">Create New</a>
        <div class="box mt-1">
            {{ $comms->links() }}
            <table class="table w-100 mt-1">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Active?</th>
                    <th>Domain</th>
                    <th>Plan</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comms as $c)
                    <tr>
                        <td>{{ $c->name }}</td>
                        <td>{{ $c->enabled ? 'Yes' : 'No' }}</td>
                        <td><a href="https://{{ $c->subdomain }}.lithiumatc.xyz" target="_blank">{{ $c->subdomain }}.lithiumatc.xyz</a></td>
                        <td>{{ $c->getPlan()->getName() }}</td>
                        <td><a href="{{ route('admin.communities.edit', ['community' => $c]) }}" class="button is-small is-link">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection
