@extends('templates.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">{{ isset($community) ? 'Editing' : 'Creating' }} a Community</h1>
        <div class="box mt-1">
            <form method="POST" action="{{ isset($community) ? route('admin.communities.update', ['community' => $community]) : route('admin.communities.store') }}">
                @csrf
                @method(isset($community) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control">
                                <input class="input{{ invalid('name', $errors) }}" type="text" name="name" placeholder="Name" value="{{ old('name') ? old('name') : (isset($community) ? $community->name : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Subdomain</label>
                            <div class="">
                                <input class="input{{ invalid('subdomain', $errors) }}" type="text" name="subdomain" placeholder="fuckyou" value="{{ old('subdomain') ? old('subdomain') : (isset($community) ? $community->subdomain : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Owner ID</label>
                            <div class="control">
                                <input class="input{{ invalid('owner_id', $errors) }}" type="text" name="owner_id" placeholder="Owner ID" value="{{ old('owner_id') ? old('owner_id') : (isset($community) ? $community->owner->discord_id : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Plan</label>
                            @php
                                $v = old('plan') ? old('plan') : (isset($community) ? $community->plan_id : '');
                            @endphp
                            <div class="select w-100{{ invalid('plan', $errors) }}">
                                <select class="w-100" name="plan">
                                    <option value="2" {{ $v == 2 ? 'selected' : '' }}>Demo</option>
                                    <option value="0" {{ $v == 0 ? 'selected' : '' }}>Standard</option>
                                    <option value="1" {{ $v == 1 ? 'selected' : '' }}>Extreme</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        @if(isset($community))
                            @if($community->enabled)
                                <input type="submit" name="action" value="Deactivate" class="button is-danger" />
                            @else
                                <input type="submit" name="action" value="Activate" class="button is-success" />
                            @endif
                        @endif
                    </div>
                    <div class="level-right">
                        <input type="submit" name="action" value="{{ isset($community) ? 'Edit' : 'Create' }}" class="button is-link" />
                    </div>
                </div>
            </form>
        </div>
        @include('components.validation')
    </section>
@endsection
