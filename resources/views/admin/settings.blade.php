@extends('templates.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">System Settings</h1>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
        <div class="box mt-1">
            <div class="level">
                <div class="level-left">
                    <strong>System-wide Banner</strong> @if($i->active) <span class="tag is-danger ml-1">Active</span> @endif
                </div>
                <div class="level-right">
                    @if(!$i->active)
                        <form method="POST" action="{{ route('admin.settings.banner.activate') }}">
                            @csrf
                            <button class="button is-danger is-small">Activate</button>
                        </form>
                    @else
                        <form method="POST" action="{{ route('admin.settings.banner.deactivate') }}">
                            @csrf
                            <button class="button is-link is-small">Deactivate</button>
                        </form>
                    @endif
                </div>
            </div>
            <form method="POST" action="{{ route('admin.settings.banner.save') }}">
                @csrf
                <div class="field">
                    <label class="label">Title</label>
                    <div class="component">
                        <input type="text" class="input{{ invalid('title', $errors) }}" name="title" value="{{ old('title') ? old('title') : $i->title }}" />
                    </div>
                </div>
                <div class="field">
                    <label class="label">Description</label>
                    <div class="component">
                        <textarea class="textarea{{ invalid('description', $errors) }}" name="description">{{ old('description') ? old('description') : $i->description }}</textarea>
                    </div>
                </div>
                <button class="button is-link">Save Banner</button>
            </form>
        </div>
    </section>
@endsection
