@php
    $iss = \App\Issue::getIssue();
@endphp

@if($iss->active)
    <div class="notification is-danger" style="text-align: center;">
        <p><strong>{{ $iss->title }}</strong></p>
        <p>{{ $iss->description }}</p>
    </div>
@endif
