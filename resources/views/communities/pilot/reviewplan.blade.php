@extends('templates.communities.pilot')

@section('pilot_body')
    <section class="section pt-0">
        <h1 class="title is-1">Reviewing Plan</h1>
        <div class="box">
            <div class="columns">
                <div class="column">
                    <strong>Flight will be credited to:</strong>
                    <ul>
                        <li>{{ $plan->pilot->name }}</li>
                        @if($plan->airline_id != null) <li>{{ $plan->airline->name }}</li> @endif
                    </ul>
                </div>
                <div class="column">
                    <strong>Flight Information</strong>
                    <br />{{ $plan->callsign }} ({{ $plan->aircraft_type }} flying from {{ $plan->origin }} to {{ $plan->destination }}{{ $plan->alternative != null ? '/' . $plan->alternative : ''}})
                    <br />Crusing Altitude and Speed: {{ $plan->altitude }} @ {{ $plan->true_airspeed }}kts
                </div>
            </div>
            <strong>Route:</strong>
            <br /><p>{{ $plan->routing }}</p>
            @if($plan->remarks != null)
                <div class="mt-1"></div>
                <strong>Remarks:</strong>
                <p>{{ $plan->remarks }}</p>
            @endif
            <div class="mt-2">
                <button onclick="document.getElementById('reject').submit()" class="button is-danger">Cancel & Edit</button> <button onclick="document.getElementById('approve').submit()" class="button is-link">File</button>
            </div>
            <form action="{{ coute('c.fileplan.review.reject', ['plan' => $plan]) }}" method="POST" style="display: none;" id="reject">@csrf</form>
            <form action="{{ coute('c.fileplan.review.approve', ['plan' => $plan]) }}" method="POST" style="display: none;" id="approve">@csrf</form>
        </div>
    </section>
@endsection
