@extends('templates.communities.pilot')



@section('pilot_body')
<kms class="slide-in-fwd-center">
    <section class="section pt-0">
        <h1 class="title is-2">Overview for: {{ Auth::user()->name }}</h1>
        <p class="subtitle">See all previous flights and current ongoing flights</p>
        @if($cf != null)
        <div class="card">
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-48x48">
                            <img src="{{ asset('img/plane-solid.svg') }}">
                        </figure>
                    </div>
                    <div class="media-content">
                        <div class="columns">
                            <div class="column">
                                <p class="title">{{ $cf->callsign }}</p>
                                <p class="subtitle">{{ $cf->aircraft_type }}</p>
                            </div>
                            <div class="column">
                                <div class="content">
                                    Flying from: {{ $cf->origin }} to {{ $cf->destination }} @if($cf->alternative != null) with {{ $cf->alternative }} as a diversion @endif
                                    <br> Cruising at {{ $cf->altitude }} and {{ $cf->true_airspeed }}kts @if($cf->airline_id != null) for {{ $cf->airline->name }} @endif
                                </div>
                            </div>
                        </div>
                        <div class="level">
                            <div class="level-left">

                            </div>
                            <div class="level-right">
                                <button class="button is-danger" onclick="document.getElementById('cancelcurrent').submit()">End Flight</button>
                                <form style="display: none;" action="{{ coute('c.pilotdash.cancel') }}" method="POST" id="cancelcurrent">@csrf</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </section>

    <!-- Show last 10 previous flights -->
    <section class="section">
        <div class="box">
            <h3 class="title is-3">Past Flights</h3>
            <table class="table w-100">
                <thead>
                <tr>
                    <th>Callsign</th>
                    <th>Aircraft ICAO</th>
                    <th>Departure</th>
                    <th>Arrival</th>
                    <th>Alternative</th>
                    <th>Cruise ALT</th>
                    <th>Airspeed</th>
                    <th>VA (If assigned)</th>
                </tr>
                </thead>
                <tbody>
                @if(count($arc) == 0)
                    <tr>
                        <td colspan="8">No past flights :(</td>
                    </tr>
                @endif
                @foreach($arc as $a)
                    <tr>
                        <td>{{ $a->callsign }}</td>
                        <td>{{ $a->aircraft_type }}</td>
                        <td>{{ $a->origin }}</td>
                        <td>{{ $a->destination }}</td>
                        <td>{{ $a->alternative }}</td>
                        <td>{{ $a->altitude }}</td>
                        <td>{{ $a->true_airspeed }}</td>
                        <td>{{ $a->airline_id == null ? 'None' : $a->airline->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
</kms>
@endsection
