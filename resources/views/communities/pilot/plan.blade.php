@extends('templates.communities.pilot')

{{--@section('header')
<style>
html{
    height: 100%;
}
body{
    background: radial-gradient(ellipse farthest-side at 76% 77%, rgba(245, 228, 212, 0.25) 4%, rgba(255, 255, 255, 0) calc(4% + 1px)), radial-gradient(circle at 76% 40%, #fef6ec 4%, rgba(255, 255, 255, 0) 4.18%), linear-gradient(135deg, #ff0000 0%, #000036 100%), radial-gradient(ellipse at 28% 0%, #ffcfac 0%, rgba(98, 149, 144, 0.5) 100%), linear-gradient(180deg, #cd6e8a 0%, #f5eab0 69%, #d6c8a2 70%, #a2758d 100%); background-blend-mode: normal, normal, screen, overlay, normal;
    height: 100%;
}
</style>
@endsection--}}

@section('pilot_body')
    <section class="section mt-0 pt-0 slide-in-fwd-center">
        <h1 class="title is-2">File a new flight plan:</h1>
        <p class="subtitle">You will be submitting a file plan to your account ({{ Auth::user()->name }})</p>
        <form action="{{ coute('c.fileplan.process') }}" method="POST" class="box">
            @csrf
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label for="va" class="label">Virtual Airline</label>
                        <div class="select w-100">
                            <select id="va" class="input{{ invalid('va', $errors) }}" name="va">
                                <option value="0" {{ old('va') == 0 ? 'selected' : '' }}>None</option>
                                @foreach($vas as $va)
                                    <option value="{{ $va->id }}" {{ old('va') == $va->id ? 'selected' : '' }}>{{ $va->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Callsign</label>
                        <div class="control">
                            <input class="input{{ invalid('callsign', $errors) }}" name="callsign" type="text" placeholder="EZS0000" value="{{ old('callsign') }}">
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Aircraft Type</label>
                        <div class="control">
                            <input class="input{{ invalid('type', $errors) }}" name="type" type="text" placeholder="A320" value="{{ old('type') }}">
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Origin</label>
                        <div class="control">
                            <input class="input{{ invalid('origin', $errors) }}" name="origin" type="text" placeholder="Departure Airport" value="{{ old('origin') }}">
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Destination</label>
                        <div class="control">
                            <input class="input{{ invalid('destination', $errors) }}" name="destination" type="text" placeholder="Arrival Airport" value="{{ old('destination') }}">
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Alternative</label>
                        <div class="control">
                            <input class="input{{ invalid('alternative', $errors) }}" name="alternative" type="text" placeholder="Alternative / Diversion" value="{{ old('alternative') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Cruising Altitude</label>
                        <div class="control">
                            <input class="input{{ invalid('altitude', $errors) }}" name="altitude" type="text" placeholder="32000" value="{{ old('altitude') }}">
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Airspeed (KTS)</label>
                        <div class="control">
                            <input class="input{{ invalid('true_airspeed', $errors) }}" name="true_airspeed" type="text" placeholder="180" value="{{ old('true_airspeed') }}">
                        </div>
                    </div>
                </div>
                <div class="column is-three-fifths">
                    <div class="field">
                        <label class="label">Route</label>
                        <div class="control">
                            <input class="input{{ invalid('route', $errors) }}" name="route" type="text" placeholder="DCT" value="{{ old('route') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Remarks</label>
                        <div class="control">
                            <textarea class="textarea{{ invalid('remarks', $errors) }}" name="remarks" rows="2">{{ old('remarks') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" name="after" value="Review" class="button is-link" />
            <input type="submit" name="after" value="Submit" class="button is-danger is-light" />
        </form>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('message') }}
                </div>
            </article>
        @endif
    </section>

@endsection
