@extends('templates.communities.pilot')

@section('pilot_body')
    <section class="pt-0 section">
        <h1 class="title is-2">Charts</h1>
        <form method="GET" action="{{ coute('c.pilot.charts') }}" id="apform">
            <div class="select">
                <select name="airport" onchange="changeAP()">
                    <option disabled {{ !isset($ap) ? 'selected' : '' }}>Select Airport</option>
                    @foreach($aps as $a)
                        <option value="{{ $a->id }}" {{ isset($ap) ? ($ap->id == $a->id ? 'selected' : '') : '' }}>{{ $a->icao }} - {{ $a->name }}</option>
                    @endforeach
                </select>
            </div>
        </form>
        @if(isset($ap))
            <div class="box mt-1">
                <strong>Charts for {{ $ap->name }}</strong>
                @if($ap->charts()->count() > 0)
                <div class="columns mt-1">
                    @if(count($ap->charts()->where('type', 'AD')->get()) > 0)
                        <div class="column">
                            <table class="table w-100">
                                <thead>
                                <tr>
                                    <th>Airport Diagrams</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ap->charts()->where('type', 'AD')->get() as $c)
                                    <tr>
                                        <td><a href="{{ $c->url }}" target="_blank">{{ $c->name }}</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                    @if(count($ap->charts()->where('type', 'SID')->get()) > 0)
                        <div class="column">
                            <table class="table w-100">
                                <thead>
                                <tr>
                                    <th>Standard Instrument Departures</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ap->charts()->where('type', 'SID')->get() as $c)
                                    <tr>
                                        <td><a href="{{ $c->url }}" target="_blank">{{ $c->name }}</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                    @if(count($ap->charts()->where('type', 'STAR')->get()) > 0)
                        <div class="column">
                            <table class="table w-100">
                                <thead>
                                <tr>
                                    <th>Standard Arrival Routes</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ap->charts()->where('type', 'STAR')->get() as $c)
                                    <tr>
                                        <td><a href="{{ $c->url }}" target="_blank">{{ $c->name }}</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
                @else
                    <p>No charts available.</p>
                @endif

            </div>
        @endif
    </section>
@endsection

@section('footer')
    <script>
        function changeAP() {
            document.getElementById('apform').submit();
        }
    </script>
@endsection
