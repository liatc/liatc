@extends('templates.app')

@section('header')
<style>
@import url('https://fonts.googleapis.com/css2?family=Muli:wght@200&display=swap');

body{
    background-color: #0086ac;
    font-family: 'Muli', sans-serif;
}
</style>
@endsection

@section('body')
<section class="section slide-in-fwd-center">

        <div class="columns">
        <div class="column"></div>
        <div class="column">
        <h2 class="title is-1 has-text-white" style="font-size: 130px;">
            :(
            </h2><br>
            <p class="subtitle is-4 has-text-white">Your community ran into an issue and has been deactivated. We're just collecting some information behind the scenes as to what went wrong, and then we'll inform your community owner.</p>
            <br>
            <p class="sbutitle has-text-white">You will need to refresh this page manually</p>
            <br>
            <div class="columns">
                <div class="column">
                    <img src="https://cdn.tobyroyal.codes/i/irzaek0uavnf2k7x.png" style="width:100%;">
                </div>
                <div class="column">
                    <p class="subtitle is-5 has-text-white"><strong class="has-text-white">For community owners:</strong> You'll need to get in touch to resolve this issue!<br><br><br>
                    Stop Code: <br><br><code>SYS_COMMUNITY_DEACTIVATED</code></p>
                </div>
            </div>
            
        </div>
        <div class="column"></div>
        </div>

</section>
<section class="section slide-in-fwd-center">
    <div class="container">
        
    </div>
</section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>



@endsection


@section('footer')

@endsection