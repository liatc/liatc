@extends('templates.communities.atc')

@section('atc_header')
    <meta name="X-API-KEY" content="{{ $token }}" />
    <meta name="X-COMMUNITY-ID" content="{{ community()->id }}" />
@endsection

@section('atc_body')
    <section class="section">
        <div id="vue-app"><ActiveApp /></div>
    </section>
@endsection

@section('atc_footer')
    <script src="{{ asset('/js/active.js') }}"></script>
@endsection
