@extends('templates.communities.atc', ['title'=>community()->name . ' | LithiumATC'])

@section('atc_body')
    <h1 class="title is-2">Greetings Controller, {{ Auth::user()->name }}:</h1>
    <p class="subtitle">See important information posted by your head of department and other
        information.</p>

    {{--<section class="section">
        <p class="subtitle">Latest news:</p>
        <div class="card">
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-48x48">
                            <img src="{{ asset('img/broadcast-tower-solid.svg') }}">
                        </figure>
                    </div>
                    <div class="media-content">
                        <p class="title">[Name of supervisor]</p>
                        <p class="subtitle">[their position]</p>
                    </div>
                    <div>

                        <div class="content">
                            [Information that they'd like shared is to be put here for all the world to see,
                            including you, yes you!]
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
@endsection
