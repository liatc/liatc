<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LithiumATC Strips</title>
    <meta name="X-API-KEY" content="{{ $token }}" />
    <meta name="X-COMMUNITY-ID" content="{{ community()->id }}" />
    <link rel="stylesheet" href="{{ asset('/css/strips.css') }}" />
</head>
<body>
    <div id="loading">Flight Strips are loading. Please wait.. <a href="{{ coute('c.atc.index') }}">Return to ATC Dashboard</a></div>
    <div id="app"><stripsapp /></div>
    <script src="{{ asset('/js/strip.js') }}"></script>
</body>
</html>
