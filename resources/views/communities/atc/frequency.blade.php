@extends('templates.communities.atc')

@section('atc_body')
    <section class="section pt-0">
        <h1 class="title is-2">Changing Frequencies</h1>
        <div class="box mt-1">
            <form method="POST" action="{{ coute('c.atc.frequency.process') }}">
                @csrf
                @method(isset($va) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Station Name</label>
                            <div class="control">
                                <input class="input{{ invalid('name', $errors) }}" type="text" name="name" placeholder="LSX_TWR" value="{{ old('name') ? old('name') : \Illuminate\Support\Facades\Auth::user()->station_name }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Frequency</label>
                            <div class="">
                                <input class="input{{ invalid('frequency', $errors) }}" type="text" name="frequency" placeholder="118.00" value="{{ old('frequency') ? old('frequency') : \Illuminate\Support\Facades\Auth::user()->frequency }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                    </div>
                    <div class="level-right">
                        <input type="submit" name="action" value="Change" class="button is-link" />
                    </div>
                </div>
            </form>
        </div>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection
