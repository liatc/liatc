@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">Extended Settings</h1>
        <div class="box">
            <p><strong>Extended Settings</strong></p>
            <p>These settings are available customization options for your current plan ({{ community()->getPlan()->getName() }}). Blank fields are treated as default values.</p>
            <div class="mt-1"></div>
            <p><strong>LATC Color Syntax</strong></p>
            <p>
                LATC Color Syntax is defined as <code>key:value</code> where <code>key</code> can be either of <code>color</code> or <code>gradient</code>.
                 Currently available gradients are <code>home</code> (<code>gradient:home</code>) and community (<code>gradient:community</code>).
                 Colors can be any hex value without the <code>#</code> prefix (<code>color:ffffff</code>).
            </p>
        </div>
        <form method="POST" action="{{ coute('c.admin.settings.save') }}">
            @csrf
            @foreach(array_keys($a) as $k)
                <div class="box">
                    <p><strong>{{ $k }}</strong></p>
                    <hr />
                    @foreach(array_keys($a[$k]) as $setting)
                        @php
                        $s = $a[$k][$setting];
                        @endphp
                        <div class="columns">
                            <div class="column is-one-quarter">
                                <strong>{{ $s['name'] }}</strong>
                            </div>
                            @if(false)

                            @else
                                <div class="column">
                                    <div class="field">
                                        <div class="control">
                                            <input class="input{{ invalid($setting, $errors) }}" type="{{ $s['type'] }}" name="{{ $setting }}" value="{{ old($setting) ? old($setting) : community()->getSetting($setting) }}" />
                                        </div>
                                        @if($s['is_color'])
                                            <small>This field uses LATC Color Syntax.</small>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endforeach
            <div class="box" style="text-align: center;">
                <button class="button is-link">Save Changes</button>
            </div>
        </form>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection
