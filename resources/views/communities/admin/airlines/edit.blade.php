@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">{{ isset($va) ? 'Editing' : 'Creating' }} an Airline</h1>
        <div class="box mt-1">
            <form method="POST" action="{{ isset($va) ? coute('c.admin.airlines.update', ['airline' => $va]) : coute('c.admin.airlines.store') }}">
                @csrf
                @method(isset($va) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control">
                                <input class="input{{ invalid('name', $errors) }}" type="text" name="name" placeholder="British Airways" value="{{ old('name') ? old('name') : (isset($va) ? $va->name : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">ICAO Callsign</label>
                            <div class="">
                                <input class="input{{ invalid('icao', $errors) }}" type="text" name="icao" placeholder="BAW" value="{{ old('icao') ? old('icao') : (isset($va) ? $va->icao : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Owner ID</label>
                            <div class="control">
                                <input class="input{{ invalid('owner_id', $errors) }}" type="text" name="owner_id" placeholder="Owner ID" value="{{ old('owner_id') ? old('owner_id') : (isset($va) ? $va->owner->discord_id : '') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <a href="{{ coute('c.admin.airlines.index') }}" class="button is-light">Back to Airlines List</a>
                    </div>
                    <div class="level-right">
                        @if(isset($va))
                        <button class="button is-danger" id="killswitch">Delete</button>
                        &nbsp;
                        @endif
                        <input type="submit" name="action" value="{{ isset($va) ? 'Edit' : 'Create' }}" class="button is-link" />
                    </div>
                </div>
            </form>
            @if(isset($va))
            <form style="display: none;" action="{{ coute('c.admin.airlines.destroy', ['airline' => $va]) }}" method="POST" id="del">
                @csrf
                @method('DELETE')
            </form>
            @endif
        </div>
        @if(isset($va))
        <div class="box mt-1">
            <div class="level">
                <div class="level-left">
                    <strong>Discord Webhooks</strong>
                </div>
                <div class="level-right">
                    <a href="{{ coute('c.admin.airlines.newhook', ['airline' => $va]) }}" class="button is-small is-link">+ Webhook</a>
                </div>
            </div>
            <table class="w-100 table">
                <thead>
                <tr>
                    <th>URL</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($va->webhooks) == 0)
                    <tr>
                        <td colspan="2">No webhooks added.</td>
                    </tr>
                @endif
                @foreach($va->webhooks as $hook)
                    <tr>
                        <td>{{ $hook->url }}</td>
                        <td><button class="button is-small is-danger" onclick="killHook({{ $hook->id }})">Remove</button></td>
                        <form style="display: none;" action="{{ coute('c.admin.airlines.delhook', ['airline' => $va, 'hook' => $hook]) }}" method="POST" id="del.{{ $hook->id }}">
                            @csrf
                            @method('DELETE')
                        </form>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection

@section('footer')
    <script>
        document.getElementById('killswitch').onclick = killWithFire;
        function killWithFire(e) {
            e.preventDefault();
            var c = confirm("Are you sure you want to delete this airline?\nAll associated flights will be deleted.");
            if(!c) return false;
            document.getElementById('del').submit();
            return false;
        }

        function killHook(id) {
            var c = confirm("Are you sure you want to delete this webhook?");
            if(!c) return false;
            document.getElementById('del.' + id).submit();
            return false;
        }
    </script>
@endsection
