@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">Virtual Airlines</h1>
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
        <a href="{{ coute('c.admin.airlines.create') }}" class="button is-link mb-1">Create Airline</a>
        <div class="box">
            {{ $airlines->links() }}
            <table class="table w-100">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>ICAO Callsign</th>
                    <th>Owner</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($airlines) == 0)
                    <tr>
                        <td colspan="4">No airlines have been created :(</td>
                    </tr>
                @endif
                @foreach($airlines as $va)
                    <tr>
                        <td>{{ $va->name }}</td>
                        <td>{{ $va->icao }}</td>
                        <td>{{ $va->owner->name }}</td>
                        <td><a class="button is-small is-link" href="{{ coute('c.admin.airlines.edit', ['airline' => $va]) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $airlines->links() }}
        </div>
    </section>
@endsection
