@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">Adding Webhook</h1>
        <div class="box mt-1">
            <strong>Webhook will be added to {{ $airline->name }}</strong>
            <form method="POST" action="{{ coute('c.admin.airlines.webhook', ['airline' => $airline]) }}" class="mt-1">
                @csrf
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">URL</label>
                            <div class="control">
                                <input class="input{{ invalid('url', $errors) }}" type="text" name="url" value="{{ old('url') ? old('url') : null }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <a href="{{ coute('c.admin.airlines.edit', ['airline' => $airline]) }}" class="button is-light">Back to Airline</a>
                    </div>
                    <div class="level-right">
                        <input type="submit" name="action" value="Add" class="button is-link" />
                    </div>
                </div>
            </form>
        </div>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection
