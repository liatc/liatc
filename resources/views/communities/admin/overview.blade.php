@extends('templates.communities.admin', ['title'=>community()->name . ' | LithiumATC'])

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">General Settings</h1>
        <div class="box">
            <form action="{{ coute('c.admin.o.save') }}" method="POST">
                @csrf
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control">
                                <input class="input" type="text" value="{{ old('name') ? old('name') : community()->name }}" name="name" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Owner</label>
                            <div class="control">
                                <input class="input" type="text" value="{{ community()->owner->name }}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Domain</label>
                            <div class="control">
                                <input class="input" type="text" value="{{ community()->subdomain }}.lithiumatc.xyz" readonly />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <small>Owner and domain information can only be modified by your service admin.</small>
                    </div>
                    <div class="level-right">
                        <button class="button is-link">Save Settings</button>
                    </div>
                </div>
            </form>
        </div>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>

@endsection
