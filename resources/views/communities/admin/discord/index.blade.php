@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">
            Discord Sync
        </h1>
        @if(community()->getSetting('discord_server_id') != null)
        <article class="message is-primary">
            <div class="message-header">
                <p>Discord Sync Connected</p>
            </div>
            <div class="message-body">
                <p>Discord Sync is connected and working properly. Server ID: {{ community()->getSetting('discord_server_id') }}</p>
                <button onclick="document.getElementById('unlink').submit()" class="button is-danger mt-1">Disconnect</button>
                <form style="display: none;" action="{{ coute('c.admin.discord.unlink') }}" method="POST" id="unlink">
                    @csrf
                    @method('DELETE')
                </form>
            </div>
        </article>
        @else
            <article class="message is-warning">
                <div class="message-header">
                    <p>Discord Sync Disconnected</p>
                </div>
                <div class="message-body">
                    <p>Discord Sync is currently disconnected. Only you have access to this community.</p>
                    <a href="{{ coute('c.admin.discord.link') }}" class="button is-link mt-1">Connect</a>
                </div>
            </article>
        @endif
        <div class="box">
            <div class="level">
                <div class="level-left">
                    <strong>Permission Sync</strong>
                </div>
                <div class="level-right">
                    <a href="{{ coute('c.admin.discord.create', ['type' => 'permission']) }}" class="button is-link is-small">Add</a>
                </div>
            </div>

            <table class="table w-100">
                <thead>
                <tr>
                    <th>Role ID</th>
                    <th>Permission</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($perms as $perm)
                    <tr>
                        <td>{{ $perm->role_id }}</td>
                        <td>{{ $perm->permission }}</td>
                        <td><a href="{{ coute('c.admin.discord.edit', ['discord' => $perm]) }}" class="button is-link is-small">Edit</a></td>
                    </tr>
                @endforeach
                @if(count($perms) == 0)
                    <tr>
                        <td colspan="2">No configured permissions</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="box">
            <div class="level">
                <div class="level-left">
                    <strong>VA Sync</strong>
                </div>
                <div class="level-right">
                    <a href="{{ coute('c.admin.discord.create', ['type' => 'va']) }}" class="button is-link is-small">Add</a>
                </div>
            </div>

            <table class="table w-100">
                <thead>
                <tr>
                    <th>Role ID</th>
                    <th>VA Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($va as $v)
                    <tr>
                        <td>{{ $v->role_id }}</td>
                        <td>{{ $v->va->name }}</td>
                        <td><a href="{{ coute('c.admin.discord.edit', ['discord' => $v]) }}" class="button is-link is-small">Edit</a></td>
                    </tr>
                @endforeach
                @if(count($va) == 0)
                    <tr>
                        <td colspan="2">No configured VAs</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </section>
@endsection
