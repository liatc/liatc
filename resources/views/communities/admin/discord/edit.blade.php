@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">{{ isset($discord) ? 'Editing' : 'Creating' }} a {{ isset($discord) ? ($discord->va_id == null ? 'Permission' : 'VA Role') : ($type == 'permission' ? 'Permission' : 'VA Role') }}</h1>
        <div class="box mt-1">
            <form method="POST" action="{{ isset($discord) ? coute('c.admin.discord.update', ['discord' => $discord]) : coute('c.admin.discord.store') }}">
                @csrf
                @method(isset($discord) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Role ID</label>
                            <div class="control">
                                <input class="input{{ invalid('role', $errors) }}" type="text" name="role" value="{{ old('role') ? old('role') : (isset($discord) ? $discord->role_id : '') }}" />
                            </div>
                        </div>
                    </div>
                    @if(isset($discord) ? ($discord->va_id == null) : $type == 'permission')
                    <div class="column">
                        <div class="field">
                            <label class="label">Permission</label>
                            <div class="control select w-100{{ invalid('permission', $errors) }}">
                                @php
                                $val = old('permission') ? old('permission') : (isset($discord) ? $discord->permission : '');
                                @endphp
                                <select class="w-100" name="permission">
                                    @foreach(\App\Permission::AVAILABLE_PERMISSIONS as $p)
                                        <option value="{{ $p }}" {{ $val == $p ? 'selected' : '' }}>{{ $p }}</option>
                                    @endforeach
                                </select>
                                {{--<input class="input{{ invalid('permission', $errors) }}" type="text" name="permission" placeholder="BAW" value="{{ old('icao') ? old('icao') : (isset($va) ? $va->icao : '') }}" />--}}
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="column">
                        <div class="field">
                            <label class="label">VA</label>
                            <div class="control select w-100{{ invalid('va', $errors) }}">
                                @php
                                    $val = old('va') ? old('va') : (isset($discord) ? $discord->va_id : '');
                                @endphp
                                <select class="w-100" name="va">
                                    @foreach(community()->virtualAirlines as $v)
                                        <option value="{{ $v->id }}" {{ $val == $v->id ? 'selected' : '' }}>{{ $v->name }}</option>
                                    @endforeach
                                </select>
                                {{--<input class="input{{ invalid('owner_id', $errors) }}" type="text" name="owner_id" placeholder="Owner ID" value="{{ old('owner_id') ? old('owner_id') : (isset($va) ? $va->owner->discord_id : '') }}" />--}}
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="level">
                    <div class="level-left">
                        <a href="{{ coute('c.admin.discord.index') }}" class="button is-light">Back to Discord Sync</a>
                    </div>
                    <div class="level-right">
                        @if(isset($discord))
                        <button class="button is-danger" id="killswitch">Delete</button>
                        &nbsp;
                        @endif
                        <input type="submit" name="action" value="{{ isset($discord) ? 'Edit' : 'Create' }}" class="button is-link" />
                    </div>
                </div>
            </form>
            @if(isset($discord))
            <form style="display: none;" action="{{ coute('c.admin.discord.destroy', ['discord' => $discord]) }}" method="POST" id="del">
                @csrf
                @method('DELETE')
            </form>
            @endif
        </div>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection

@section('footer')
    <script>
        document.getElementById('killswitch').onclick = killWithFire;
        function killWithFire(e) {
            e.preventDefault();
            var c = confirm("Are you sure you want to delete this?\nUser permissions will not be reloaded until they sign back in.");
            if(!c) return false;
            document.getElementById('del').submit();
            return false;
        }
    </script>
@endsection
