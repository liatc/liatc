@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">{{ isset($ap) ? 'Editing' : 'Creating' }} an Airport</h1>
        <div class="box mt-1">
            <form method="POST" action="{{ isset($ap) ? coute('c.admin.airports.update', ['airport' => $ap]) : coute('c.admin.airports.store') }}">
                @csrf
                @method(isset($ap) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">ICAO</label>
                            <div class="control">
                                <input class="input{{ invalid('icao', $errors) }}" type="text" name="icao" placeholder="KLAX" value="{{ old('icao') ? old('icao') : (isset($ap) ? $ap->icao : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control">
                                <input class="input{{ invalid('name', $errors) }}" type="text" name="name" placeholder="Los Angeles Int'l Airport" value="{{ old('name') ? old('name') : (isset($ap) ? $ap->name : '') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <a href="{{ coute('c.admin.airports.index') }}" class="button is-light">Back to Airports List</a>
                    </div>
                    <div class="level-right">
                        @if(isset($ap))
                        <button class="button is-danger" id="killswitch">Delete</button>
                        &nbsp;
                        @endif
                        <input type="submit" name="action" value="{{ isset($ap) ? 'Edit' : 'Create' }}" class="button is-link" />
                    </div>
                </div>
            </form>
            @if(isset($ap))
            <form style="display: none;" action="{{ coute('c.admin.airports.destroy', ['airport' => $ap]) }}" method="POST" id="del">
                @csrf
                @method('DELETE')
            </form>
            @endif
        </div>
        @if(isset($ap))
            <div class="box mt-1">
                <div class="level">
                    <div class="level-left">
                        <strong>Charts</strong>
                    </div>
                    <div class="level-right">
                        <a href="{{ coute('c.admin.airports.charts.create', ['airport' => $ap]) }}" class="button is-small is-link">+ Chart</a>
                    </div>
                </div>
                <table class="table w-100">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($ap->charts) == 0)
                        <tr>
                            <td colspan="3">No Charts for Airport</td>
                        </tr>
                    @endif
                    @foreach($ap->charts as $chart)
                        <tr>
                            <td>{{ $chart->type }}</td>
                            <td><a href="{{ $chart->url }}" target="_blank">{{ $chart->name }}</a></td>
                            <td><a class="button is-small is-link" href="{{ coute('c.admin.airports.charts.edit', ['airport' => $chart->airport, 'chart' => $chart]) }}">Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection

@section('footer')
    <script>
        document.getElementById('killswitch').onclick = killWithFire;
        function killWithFire(e) {
            e.preventDefault();
            var c = confirm("Are you sure you want to delete this airport?\nAll associated charts will be deleted.");
            if(!c) return false;
            document.getElementById('del').submit();
            return false;
        }
    </script>
@endsection
