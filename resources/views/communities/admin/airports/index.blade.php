@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">Airports</h1>
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
        <a href="{{ coute('c.admin.airports.create') }}" class="button is-link mb-1">Create Airport</a>
        <div class="box">
            {{ $aps->links() }}
            <table class="table w-100">
                <thead>
                <tr>
                    <th>ICAO</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($aps) == 0)
                    <tr>
                        <td colspan="4">No airports have been created :(</td>
                    </tr>
                @endif
                @foreach($aps as $a)
                    <tr>
                        <td>{{ $a->icao }}</td>
                        <td>{{ $a->name }}</td>
                        <td><a class="button is-small is-link" href="{{ coute('c.admin.airports.edit', ['airport' => $a]) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $aps->links() }}
        </div>
    </section>
@endsection
