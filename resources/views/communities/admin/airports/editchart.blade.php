@extends('templates.communities.admin')

@section('admin_body')
    <section class="section pt-0">
        <h1 class="title is-2">{{ isset($c) ? 'Editing' : 'Creating' }} a Chart</h1>
        <div class="box mt-1">
            @if(isset($c))
                <strong>Chart added to {{ $c->airport->name }}</strong>
            @else
                <strong>Chart will be added to {{ $ap->name }}</strong>
            @endif
            <form method="POST" class="mt-1" action="{{ isset($c) ? coute('c.admin.airports.charts.update', ['chart' => $c, 'airport' => $c->airport]) : coute('c.admin.airports.charts.store', ['airport' => $ap]) }}">
                @csrf
                @method(isset($c) ? 'PATCH' : 'POST')
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label class="label">Type</label>
                            <div class="select{{invalid('type', $errors)}} w-100">
                                @php
                                $val = old('type') ? old('type') : (isset($c) ? $c->type : '');
                                @endphp
                                <select name="type" class="w-100">
                                    <option value="ENRT" {{ $val == "ENRT" ? 'selected' : '' }}>Enroute</option>
                                    <option value="AD" {{ $val == "AD" ? 'selected' : '' }}>Airport Diagram</option>
                                    <option value="SID" {{ $val == "SID" ? 'selected' : '' }}>SID</option>
                                    <option value="STAR" {{ $val == "STAR" ? 'selected' : '' }}>STAR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control">
                                <input class="input{{ invalid('name', $errors) }}" type="text" name="name" placeholder="Airport Diagram" value="{{ old('name') ? old('name') : (isset($c) ? $c->name : '') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label class="label">URL</label>
                            <div class="control">
                                <input class="input{{ invalid('url', $errors) }}" type="text" name="url" placeholder="https://www.google.com" value="{{ old('url') ? old('url') : (isset($c) ? $c->url : '') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <a href="{{ coute('c.admin.airports.edit', ['airport' => isset($c) ? $c->airport : $ap]) }}" class="button is-light">Back to Airport</a>
                    </div>
                    <div class="level-right">
                        @if(isset($c))
                        <button class="button is-danger" id="killswitch">Delete</button>
                        &nbsp;
                        @endif
                        <input type="submit" name="action" value="{{ isset($c) ? 'Edit' : 'Create' }}" class="button is-link" />
                    </div>
                </div>
            </form>
            @if(isset($c))
            <form style="display: none;" action="{{ coute('c.admin.airports.charts.delete', ['airport' => $c->airport, 'chart' => $c]) }}" method="POST" id="del">
                @csrf
                @method('DELETE')
            </form>
            @endif
        </div>
        @include('components.validation')
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ \Illuminate\Support\Facades\Session::get('success_message') }}
                </div>
            </article>
        @endif
    </section>
@endsection

@section('footer')
    <script>
        document.getElementById('killswitch').onclick = killWithFire;
        function killWithFire(e) {
            e.preventDefault();
            var c = confirm("Are you sure you want to delete this chart?");
            if(!c) return false;
            document.getElementById('del').submit();
            return false;
        }
    </script>
@endsection
