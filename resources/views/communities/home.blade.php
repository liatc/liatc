@extends('templates.community', ['title'=>community()->name . ' | LithiumATC'])

@section('header')
<style>
body{
{{ \App\Community::resolveColor(community()->customization['backgrounds-home'], true) }}
}
</style>
@endsection

@section('pub_body')
<section class="section"></section>
<section class="section slide-in-fwd-center">
    <div class="container">
        <h2 class="title is-1 has-text-white">
        {{ community()->name }}
        </h2>
        <p class="subtitle has-text-white">Powered by: LithiumATC {{ community()->getPlan()->getName() }}</p>
        <br><br>
        @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('pilot'))
        <a href="{{ coute('c.pilotdash') }}" class="button is-link">Pilot Dashboard</a>
        @endif
        @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('atc'))
        <a href="{{ coute('c.atc.index') }}" class="button is-link">ATC Dashboard</a>
        @endif
    </div>
</section>
<section class="section slide-in-fwd-center">
    <div class="container">
        <p class="subtitle has-text-white">
        <strong class="has-text-white">Current Users Online:</strong> {{ community()->getOnlineUsers() }}
        </p>

        <p class="subtitle has-text-white">
        <strong class="has-text-white">Active ATC Online:</strong> {{ \App\Presence::where('community_id', community()->id)->count() }}
        </p>
    </div>
</section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>



@endsection


@section('footer')

@endsection
