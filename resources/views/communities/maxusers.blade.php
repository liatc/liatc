@extends('templates.app')

@section('header')
<style>
@import url('https://fonts.googleapis.com/css2?family=Lato:wght@300&display=swap');

body{
    background-color: #e63232;
    font-family: 'Lato', sans-serif;
}
</style>
@endsection

@section('body')
<section class="section slide-in-fwd-center">
</section>
<section class="section slide-in-fwd-center">
<div class="container">
        <!-- div class="columns">
        <div class="column"></div>
        <div class="column">-->
        <h2 class="title is-1 has-text-white" style="font-size: 80px;">
            <txthere id="errmsg"></txthere>
            </h2><br>
            <p class="subtitle is-4 has-text-white">The maximum limit of concurrent users online has been reached! Try again later to see if there is space.</p>
            <br>
            <p class="sbutitle has-text-white">This plan supports a limit of {{ community()->getPlan()->getMaxConcurrent() }} users online at a time.</p>
            <br><br><br>
                    <p class="subtitle is-5 has-text-white"><strong class="has-text-white">If you are a community owner:</strong> Your current plan on LithiumATC limiting your users. <a style="text-decoration: underline;" class="has-text-white" href="https://lithiumatc.xyz/pricing">Consider upgrading</a>.<br><br><br>
                    <br><code>USER_LIMIT_REACHED</code></p>
            
        <!--</div>
        <div class="column"></div>
        </div>-->
</div>
</section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>



@endsection


@section('footer')
<script>

const words = ["Dang", "Oh no!", "That's an issue..", "Shoot!", "Oh dear..", "This ain't good chief!", "Ah crap!", "Uh oh..", "Oh noes!", "Error!", "404- Wait no it's just an error"];
const randomWords = words[Math.floor(Math.random() * words.length)];

console.log("random words today are =>", randomWords);
document.getElementById("errmsg").innerHTML = randomWords;

</script>
@endsection