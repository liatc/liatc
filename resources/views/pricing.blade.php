@extends('templates.public')

@section('header')
<style>
body{
    background: radial-gradient(100% 225% at 100% 0%, #FAFF00 0%, #000000 100%), linear-gradient(235deg, #DB00FF 0%, #000000 100%), linear-gradient(45deg, #241E92 0%, #241E92 40%, #5432D3 40%, #5432D3 50%, #7B6CF6 50%, #7B6CF6 70%, #E5A5FF 70%, #E5A5FF 100%), linear-gradient(180deg, #01024E 0%, #01024E 43%, #543864 43%, #543864 62%, #8B4367 62%, #8B4367 80%, #FF6464 80%, #FF6464 100%);
background-blend-mode: overlay, hard-light, overlay, normal;
}
</style>
@endsection
@section('pub_body')
<section class="section"></section>
<section class="section">
    <div class="container">
        <h2 class="title is-2 has-text-white">
        LithiumATC Pricing
        </h2>
        <p class="subtitle has-text-white">LithiumATC Pricing & Features</p>
    </div>
</section>
<section class="section">
<div class="container">
    <div class="columns">
        <div class="column">
            <div class="box">
                <p class="title is-3">LATC: Standard Edition</p>
                <p class="subtitle is-5 has-text-dark"><strong class="subtitle is-4">7 CHF</strong> / monthly
                <br>
                <p class="subtitle">This plan includes the following features:<br>
                - Custom community subdomain<br>
                - Multiple VA support<br>
                - Discord permissions checking<br>
                - Full ATC Dashboard control<br>
                - <strong>50 Concurrent users allowed</strong><br><br>
                </p>
                <button style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em" id="checkout-button-plan_H62TXAyNG2ZUZ7" role="link">
                     Purchase
                </button>

                <div id="error-message"></div>


            </div>
        </div>
        <div class="column">
        <div class="box">
                <p class="title is-3">LATC: Extreme Edition</p>
                <p class="subtitle is-5 has-text-dark"><strong class="subtitle is-4">12 CHF</strong> / monthly
                <br>
                <p class="subtitle">This plan includes the following features:<br>
                - All features for standard addition<br>
                - Customization of branding and colors<br>
                - Unlimited concurrent users<br>
                - Flight Strips (With Handoff Ability)<br>
                - A free cookie*<br><br>
                </p>
                <button style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em" id="checkout-button-plan_H7VEvM1xzxBHDq" role="link">
                     Purchase
                </button>

                <div id="error-message"></div>


            </div>
        </div>
    </div>
    <p class="has-text-white">*Cookies are not included unfortunately.</p>
</div>
</section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>
<section class="section"></section>


@endsection

@section('footer')
<script defer src="https://greenprint.tobyroyal.codes/js/all.js"></script>

<!-- Load Stripe.js on your website. -->
<script src="https://js.stripe.com/v3"></script>


<!-- Checkout Scripts -->
<script>
(function() {
  var stripe = Stripe('pk_live_TwuvFiw0AGvjUh3pqG7wrD2i00osEEbsy8');

  var checkoutButton = document.getElementById('checkout-button-plan_H62TXAyNG2ZUZ7');
  checkoutButton.addEventListener('click', function () {
    // When the customer clicks on the button, redirect
    // them to Checkout.
    stripe.redirectToCheckout({
      items: [{plan: 'plan_H62TXAyNG2ZUZ7', quantity: 1}],

      // Do not rely on the redirect to the successUrl for fulfilling
      // purchases, customers may not always reach the success_url after
      // a successful payment.
      // Instead use one of the strategies described in
      // https://stripe.com/docs/payments/checkout/fulfillment
      successUrl: 'https://checkout.pontifex.tech/success',
      cancelUrl: 'https://checkout.pontifex.tech/canceled',
    })
    .then(function (result) {
      if (result.error) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer.
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
  });
})();
</script>

<script>
    (function() {
        var stripe = Stripe('pk_live_TwuvFiw0AGvjUh3pqG7wrD2i00osEEbsy8');

        var checkoutButton = document.getElementById('checkout-button-plan_H7VEvM1xzxBHDq');
        checkoutButton.addEventListener('click', function () {
            // When the customer clicks on the button, redirect
            // them to Checkout.
            stripe.redirectToCheckout({
                items: [{plan: 'plan_H7VEvM1xzxBHDq', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://checkout.pontifex.tech/success',
                cancelUrl: 'https://checkout.pontifex.tech/canceled',
            })
                .then(function (result) {
                    if (result.error) {
                        // If `redirectToCheckout` fails due to a browser or network
                        // error, display the localized error message to your customer.
                        var displayError = document.getElementById('error-message');
                        displayError.textContent = result.error.message;
                    }
                });
        });
    })();
</script>

@endsection
