<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api', 'community', 'perm:atc'])->group(function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::resource('flights', 'Communities\ATC\FlightsController');
    Route::middleware('feature:flight-strips')->group(function() {
        Route::post('/flights/{flight}/handoff', 'Communities\ATC\FlightsController@handoff');
        Route::post('/flights/{flight}/handoff/cancel', 'Communities\ATC\FlightsController@cancelHandoff');
        Route::post('/flights/{flight}/handoff/accept', 'Communities\ATC\FlightsController@acceptHandoff');
        Route::post('/flights/{flight}/claim', 'Communities\ATC\FlightsController@claim');
        Route::post('/flights/{flight}/restore', 'Communities\ATC\FlightsController@restore');
        Route::get('/search/flights', 'Communities\ATC\FlightsController@search');

        Route::get('/scratchpad', 'Communities\ATC\ScratchpadController@getScratchpad');
        Route::patch('/scratchpad', 'Communities\ATC\ScratchpadController@submitContents');
        Route::post('/scratchpad/lock', 'Communities\ATC\ScratchpadController@lockScratchpad');

        Route::get('/messages', 'Communities\ATC\ChatController@getMessages');
        Route::post('/messages', 'Communities\ATC\ChatController@postMessage');

        Route::get('/airports', 'Communities\ATC\AirportsController@getAirports');
        Route::get('/charts/{airport}', 'Communities\ATC\AirportsController@getCharts');
    });
});
Route::middleware(['community'])->get('/controllers', function() {
    $f = community()->controllerPresence;
    $ret = [];
    foreach($f as $a) {
        $u = $a->user;
        $name = $u->station_name;
        if($u->station_name == null) $name = $u->name;
        array_push($ret, ['id' => $a->id, 'name' => $name, 'frequency' => $u->frequency]);
    }
    return $ret;
});

Route::post('/pusher/presence', 'PusherController@pusherPresence');

Route::middleware('apik:permissions')->post('/ingest/permissions', 'IngestController@permissions');
