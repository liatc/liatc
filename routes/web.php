<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$url_parameters = @explode(".", $_SERVER['HTTP_HOST']);

Route::domain(env('APP_ENV') == 'production' ? 'lithiumatc.xyz' : env('APP_DOMAIN', 'localhost'))->group(function() {
    Route::get('/', 'HomeController@home')->name('home');
    Route::get('/pricing', 'HomeController@pricing')->name('pricing');
    Route::get('/community', 'HomeController@commRedir')->name('communityredir');
    Route::get('/sessionforward', 'AuthController@forward')->name('login.forward');
    Route::middleware(['auth', 'sysadmin'])->prefix('/admin')->name('admin.')->group(function() {
        Route::get('/', 'Admin\MainController@home')->name('index');
        Route::get('/commRedir', function() {
            return redirect()->route('admin.communities.index');
        })->name('communitie');
        Route::resource('/communities', 'Admin\CommunitiesController');
        Route::get('/settings', 'Admin\MainController@settings')->name('settings');

        Route::post('/settings/banner/activate', 'Admin\MainController@activateBanner')->name('settings.banner.activate');
        Route::post('/settings/banner/deactivate', 'Admin\MainController@deactivateBanner')->name('settings.banner.deactivate');
        Route::post('/settings/banner', 'Admin\MainController@saveBanner')->name('settings.banner.save');
    });
    Route::middleware('auth')->get('/discord/link', 'Communities\Admin\DiscordSyncController@link')->name('discord.link');
});


Route::get('/login', 'AuthController@login')->middleware('guest')->name('login');
Route::get('/login/cb', 'AuthController@loginCb')->name('login.cb');
Route::post('/logout', 'AuthController@logout')->name('logout');

Route::domain('{community}.' . env('APP_DOMAIN'))->middleware(['community'])->group(function() {
    Route::middleware('auths')->name('c.')->group(function() {
        Route::get('/', 'Communities\HomeController@home')->name('home');
        Route::middleware('concurrent')->group(function() {
            Route::middleware('perm:pilot')->prefix('/pilot')->group(function() {
                Route::get('/', 'Communities\PilotController@pilotdash')->name('pilotdash');
                Route::get('/file', 'Communities\PilotController@fileplan')->name('fileplan');
                Route::post('/file', 'Communities\PilotController@processFile')->name('fileplan.process');
                Route::get('/file/review/{plan}', 'Communities\PilotController@reviewPlan')->name('fileplan.review');
                Route::post('/file/review/{plan}/edit', 'Communities\PilotController@cancelReview')->name('fileplan.review.reject');
                Route::post('/file/review/{plan}/file', 'Communities\PilotController@file')->name('fileplan.review.approve');
                Route::post('/cancel', 'Communities\PilotController@cancelCurrent')->name('pilotdash.cancel');

                Route::get('/charts', 'Communities\PilotController@getCharts')->name('pilot.charts');
            });
            Route::middleware(['perm:atc', 'feature:atc'])->prefix('/atc')->group(function() {
                Route::get('/', 'Communities\ATCController@atcdash')->name('atc.index');
                Route::get('/strips', 'Communities\ATCController@flightstrips')->middleware('feature:flight-strips')->name('atc.strips');
                Route::get('/active', 'Communities\ATCController@active')->name('atc.active');
                Route::get('/frequency', 'Communities\ATCController@changeFreq')->name('atc.frequency');
                Route::post('/frequency', 'Communities\ATCController@processFreqChange')->name('atc.frequency.process');
            });
            Route::middleware('perm:admin')->prefix('/admin')->name('admin.')->group(function() {
                Route::get('/', 'Communities\Admin\MainController@overview')->name('index');
                Route::post('/', 'Communities\Admin\MainController@saveOverview')->name('o.save');
                Route::resource('/airlines', 'Communities\Admin\AirlineController');
                Route::get('/airlines/{airline}/webhook', 'Communities\Admin\AirlineController@newHook')->name('airlines.newhook');
                Route::post('/airlines/{airline}/webhook', 'Communities\Admin\AirlineController@addHook')->name('airlines.webhook');
                Route::delete('/airlines/{airline}/webhook/{hook}', 'Communities\Admin\AirlineController@delHook')->name('airlines.delhook');
                Route::resource('/discord', 'Communities\Admin\DiscordSyncController');
                Route::get('/discordlink', 'Communities\Admin\DiscordSyncController@startLink')->name('discord.link');
                Route::delete('/discordlink', 'Communities\Admin\DiscordSyncController@unlink')->name('discord.unlink');
                Route::resource('/airports', 'Communities\Admin\AirportsController');
                Route::get('/airports/{airport}/charts/new', 'Communities\Admin\AirportsController@newChart')->name('airports.charts.create');
                Route::post('/airports/{airport}/charts', 'Communities\Admin\AirportsController@storeChart')->name('airports.charts.store');
                Route::get('/airports/{airport}/charts/{chart}/edit', 'Communities\Admin\AirportsController@editChart')->name('airports.charts.edit');
                Route::patch('/airports/{airport}/charts/{chart}', 'Communities\Admin\AirportsController@updateChart')->name('airports.charts.update');
                Route::delete('/airports/{airport}/charts/{chart}', 'Communities\Admin\AirportsController@deleteChart')->name('airports.charts.delete');

                Route::get('/settings', 'Communities\Admin\MainController@settings')->middleware('feature:customize')->name('settings');
                Route::post('/settings', 'Communities\Admin\MainController@saveSettings')->middleware('feature:customize')->name('settings.save');
            });
        });

    });
    Route::get('/suspended', 'Communities\HomeController@suspended')->name('suspended');
    Route::get('/userlimit', 'Communities\HomeController@userlimit')->name('userlimit');
    Route::get('/sessionforward', 'AuthController@forwardPrc')->middleware('guest')->name('login.forwardprc');
    //\Illuminate\Support\Facades\Broadcast::routes();
});


