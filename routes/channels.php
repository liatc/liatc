<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('c.{community}.flights', function ($user, $community) {
    return community()->id == $community && $user->hasPermission('atc') ? ['id' => $user->id, 'name' => $user->name, 'frequency' => $user->frequency, 'station_name' => $user->station_name] : false;
});

Broadcast::channel('c.{community}.chat', function($user, $community) {
    return community()->id == $community && $user->hasPermission('atc');
});
