<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    protected $fillable = ['community_id', 'user_id'];
    protected $table = 'presence';

    public function community() {
        return $this->belongsTo('App\Community');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
