<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $fillable = ['title', 'description', 'active'];

    public static function getIssue() {
        $i = Issue::first();
        if($i == null) {
            return Issue::create();
        }
        return $i;
    }
}
