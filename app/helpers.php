<?php

function community() {
    $h = explode('.', $_SERVER["HTTP_HOST"]);
    $sudm = $h[0];
    return \App\Community::where('subdomain', $sudm)->first();
}

function coute($route, $params = []) {
    return route($route, array_merge(['community' => community()->subdomain], $params));
}

function active($route) {
    return strpos(\Illuminate\Support\Facades\Route::current()->getName(), $route) === 0 ? ' is-active' : null;
}

function invalid($field, $errors) {
    if($errors->has($field)) return ' is-danger';
    return '';
}
