<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public const AVAILABLE_PERMISSIONS = ['pilot', 'atc', 'admin'];
    protected $fillable = ['user_id', 'community_id', 'permission'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function permission() {
        return $this->belongsTo('App\Permission');
    }
}
