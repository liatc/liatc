<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'discord_id', 'api_token', 'frequency', 'station_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function permissions() {
        return $this->hasMany('App\Permission')->where('community_id', community()->id);
    }

    public function commPerms() {
        return $this->permissions()->where('community_id', community()->id)->get();
    }

    public function flights() {
        return $this->hasMany('App\Flight', 'pilot_id')->where('community_id', community()->id);
    }

    public function hasPermission($perm) {
        $p = $this->commPerms();
        if(community()->owner_id == $this->id) return true;
        foreach($p as $pe) {
            if($pe->permission == $perm) return true;
        }
        return false;
    }

    public function getActiveFlightAttribute() {
        return community()->flights()->where('pilot_id', Auth::user()->id)->where('active', true)->first();
    }

    public function genApiToken() {
        $token = bin2hex(random_bytes(20));
        $this->api_token = $token;
        $this->save();
        return $token;
    }

    public function airlines() {
        return $this->belongsToMany('App\VirtualAirline', 'user_va', 'user_id', 'va_id')->where('community_id', community()->id);
    }

    public function syncPerms($roles, $cid) {
        $f = DB::delete("DELETE FROM `user_va` WHERE `user_id`=? AND `cm_id`=? ", [$this->id, $cid]);
        foreach($roles as $a) {
            $ds = Community::find($cid)->discordSync()->where('role_id', $a)->get();
            foreach($ds as $d) {
                if($d->permission != null) {
                    if(Permission::where('community_id', $cid)->where('user_id', $this->id)->where('permission', $d->permission)->first() != null) continue;
                    Permission::create(['user_id' => $this->id, 'permission' => $d->permission, 'community_id' => $cid]);
                }
                if($d->va_id != null) {
                    $h = DB::selectOne("SELECT `id` FROM `user_va` WHERE `user_id`=? AND `cm_id`=?", [$this->id, $cid]);
                    if($h == null)
                        DB::insert("INSERT INTO `user_va` (`user_id`, `va_id`, `cm_id`) VALUES(?, ?, ?)", [$this->id, $d->va_id, $cid]);
                }
            }
        }
    }
}
