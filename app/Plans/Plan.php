<?php


namespace App\Plans;


abstract class Plan
{
    protected $name;
    protected function __construct__() {
    }

    public abstract function getName();

    /**
     * Returns true when the plan has access to a custom subdomain.
     * @return bool
     */
    public abstract function getCanAssignSubdomain();

    /**
     * Returns the maximum number of VAs the plan has access to, or -1 if no limit exists
     * @return integer
     */
    public abstract function getMaxVA();

    /**
     * Returns true if the plan has access to the ATC add-on
     * @return bool
     */
    public abstract function getCanUseATC();

    /**
     * Returns true if the plan can link a Discord server
     * @return bool
     */
    public abstract function getCanLinkDiscord();

    /**
     * Returns the maximum number of concurrent users this plan has access to, or -1 if no limit exists
     * @return integer
     */
    public abstract function getMaxConcurrent();

    /**
     * Returns true if the plan has access to customization options
     * @return bool
     */
    public abstract function getCanCustomize();

    /**
     * Returns true if the plan has access to flight strips
     * @return bool
     */
    public abstract function getCanUseFlightStrips();

    /**
     * Returns true if the plan has access to the one chocolate chip cookie :)
     * @return bool
     */
    public abstract function hasCookie();

}
