<?php


namespace App\Plans;


class DemoPlan extends Plan
{

    public function __construct__()
    {

    }

    /**
     * @inheritDoc
     */
    public function getCanAssignSubdomain()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxVA()
    {
        return 5;
    }

    /**
     * @inheritDoc
     */
    public function getCanUseATC()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCanLinkDiscord()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxConcurrent()
    {
        return 3;
    }

    /**
     * @inheritDoc
     */
    public function getCanCustomize()
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function hasCookie()
    {
        return false;
    }

    public function getName()
    {
        return "Demo";
    }

    public function getCanUseFlightStrips()
    {
        return false;
    }
}
