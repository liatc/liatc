<?php


namespace App\Plans;


class ExtremePlan extends Plan
{

    public function __construct__()
    {
        $this->name = "Extreme";
    }

    /**
     * @inheritDoc
     */
    public function getCanAssignSubdomain()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxVA()
    {
        return -1;
    }

    /**
     * @inheritDoc
     */
    public function getCanUseATC()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCanLinkDiscord()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxConcurrent()
    {
        return -1;
    }

    /**
     * @inheritDoc
     */
    public function getCanCustomize()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function hasCookie()
    {
        return true;
    }

    public function getName()
    {
        return "Extreme";
    }

    public function getCanUseFlightStrips()
    {
        return true;
    }
}
