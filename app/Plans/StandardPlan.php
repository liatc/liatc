<?php


namespace App\Plans;


class StandardPlan extends Plan
{

    public function __construct__()
    {

    }

    /**
     * @inheritDoc
     */
    public function getCanAssignSubdomain()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxVA()
    {
        return -1;
    }

    /**
     * @inheritDoc
     */
    public function getCanUseATC()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCanLinkDiscord()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMaxConcurrent()
    {
        return 50;
    }

    /**
     * @inheritDoc
     */
    public function getCanCustomize()
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function hasCookie()
    {
        return false;
    }

    public function getName()
    {
        return "Standard";
    }

    public function getCanUseFlightStrips()
    {
        return false;
    }
}
