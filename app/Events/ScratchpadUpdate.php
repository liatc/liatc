<?php

namespace App\Events;

use App\Scratchpad;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ScratchpadUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $scratchpad;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Scratchpad $s)
    {
        $this->scratchpad = $s;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('c.' . $this->scratchpad->community_id . '.flights');
    }

    public function broadcastAs() {
        return 'scratchpad.update';
    }
}
