<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['community_id', 'user_id'];
    protected $table = "activity";
}
