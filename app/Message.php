<?php

namespace App;

use App\Events\ChatMessage;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'community_id', 'message'];
    protected $appends = ['user_name'];
    protected $dispatchesEvents = [
        'created' => ChatMessage::class
    ];

    public function getUserNameAttribute() {
        return User::find($this->user_id)->name;
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function community() {
        return $this->belongsTo('App\Community');
    }
}
