<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $fillable = ['community_id', 'icao', 'name'];

    public function community() {
        return $this->belongsTo('App\Community');
    }

    public function charts() {
        return $this->hasMany('App\Chart');
    }
}
