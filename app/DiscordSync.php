<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscordSync extends Model
{
    protected $table = "discord_sync";
    protected $fillable = ['community_id', 'role_id', 'va_id', 'permission'];

    public function va() {
        return $this->belongsTo('App\VirtualAirline', 'va_id');
    }
}
