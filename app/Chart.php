<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $fillable = ['airport_id', 'type', 'name', 'url'];

    public function airport() {
        return $this->belongsTo('App\Airport');
    }
}
