<?php

namespace App\Console\Commands;

use App\Activity;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CleanupActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes activities dangling for more than ten minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Activity::where('updated_at', '<', Carbon::now()->subMinutes(10));
    }
}
