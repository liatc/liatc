<?php

namespace App\Console\Commands;

use App\Flight;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CleanupFlights extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flights:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans up flights dangling for more than two hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Flight::where('updated_at', '<', Carbon::now()->subHours(2))->delete();
    }
}
