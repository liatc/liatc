<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['community_id', 'setting', 'value'];

    public function community() {
        return $this->belongsTo('App\Community');
    }
}
