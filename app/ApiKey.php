<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
    protected $fillable = ['key', 'purpose'];

    public static function genKey($purpose) {
        $key = bin2hex(random_bytes(20));
        $hash = hash('sha256', $key);
        $k = ApiKey::create(['key' => $hash, 'purpose' => $purpose]);
        return $key;
    }
}
