<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    protected $fillable = ['type', 'community_id', 'object_id', 'url'];

    public const WEBHOOK_VIRTUAL_AIRLINE = 0;

    public function community() {
        return $this->belongsTo('App\Community');
    }
}
