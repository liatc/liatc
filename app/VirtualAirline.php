<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VirtualAirline extends Model
{
    protected $fillable = ['name', 'icao', 'owner_id', 'community_id'];

    public function owner() {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function community() {
        return $this->belongsTo('App\Community');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_va', 'user_id', 'va_id');
    }

    public function webhooks() {
        return $this->community->webhooks()->where('type', Webhook::WEBHOOK_VIRTUAL_AIRLINE)->where('object_id', $this->id);
    }
}
