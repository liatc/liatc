<?php

namespace App\Http\Controllers;

use App\Community;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        $c = Community::where('enabled', true)->get();
        return view('home', ['communities' => $c]);
    }

    public function pricing() {
        return view('pricing');
    }

    public function admin() {
        return view('admin');
    }

    public function commRedir(Request $r) {
        $this->validate($r, [
            'cm' => 'required|integer'
        ]);
        $cm = Community::find($r->get('cm'));
        return redirect('https://' . $cm->subdomain . '.' . env('APP_DOMAIN'));
    }
}
