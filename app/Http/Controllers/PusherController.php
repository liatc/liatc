<?php

namespace App\Http\Controllers;

use App\Presence;
use Illuminate\Http\Request;

class PusherController extends Controller
{
    public function pusherPresence(Request $request) {
        if($request->header('X-Pusher-Key') != env('PUSHER_APP_KEY')) return abort(403);
        $c = $request->getContent();
        $sig = hash_hmac('sha256', $c, env('PUSHER_APP_SECRET'));
        if($sig != $request->header('X-Pusher-Signature')) return abort(403);
        $cnt = json_decode($c);
        foreach($cnt->events as $e) {
            $s = preg_match('/c.[0-9]*.flights/', $e->channel);
            if($s === 0) continue;
            $cid = explode('.', $e->channel)[1];
            switch($e->name) {
                case "member_added":
                    Presence::create(['user_id' => $e->user_id, 'community_id' => $cid]);
                    break;
                case "member_removed":
                    Presence::where('user_id', $e->user_id)->where('community_id', $cid)->delete();
                    break;
            }
        }
        return response(null, 204);
    }
}
