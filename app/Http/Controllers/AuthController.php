<?php

namespace App\Http\Controllers;

use App\Community;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function login(Request $request) {
        $cb = Community::find($request->get('cb'));
        if($cb != null) {
            Session::put('login_cb', $cb->id);
        }
        return Socialite::with('discord')->scopes(['identify', 'email', 'guilds'])->redirect();
    }

    public function loginCb() {
        $user = Socialite::driver('discord')->user();
        $accessTokenResponseBody = $user->accessTokenResponseBody;
        $u = User::where('discord_id', $user->user['id'])->first();
        if($u == null) {
            $u = User::create(['name' => $user->getNickname(), 'email' => $user->getEmail(), 'discord_id' => $user->user['id']]);
        }
        Auth::login($u);
        if(Session::has('login_cb')) {
            $cb = Community::find(Session::get('login_cb'));
            Session::remove('login_cb');
            return redirect(route('c.home', ['community' => $cb->subdomain]));
        }
        return redirect()->intended('/');
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('home');
    }

    public function forward(Request $request) {
        $c = Community::find($request->get('client'));
        if($c == null) abort(404);
        if(!Auth::check()) return redirect()->route('login', ['cb' => $c->id]);
        return redirect('https://' . $c->subdomain . '.'. env('APP_DOMAIN') . '/sessionforward?sid=' . urlencode(Session::getId()));
    }

    public function forwardPrc(Request $request) {
        Session::setId($request->get('sid'));
        Session::start();
        return redirect('/');
    }
}
