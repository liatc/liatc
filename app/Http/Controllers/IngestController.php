<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IngestController extends Controller
{
    public function permissions(Request $request) {
        $perms = json_decode($request->getContent(), true);
        $guilds = array_keys($perms);
        foreach($guilds as $g) {
            $s = Setting::where('setting', 'discord_server_id')->where('value', $g)->first();
            if($s == null) continue;
            $comm = $s->community;
            $mems = array_keys($perms[$g]);
            foreach($mems as $m) {
                $u = User::where('discord_id', $m)->first();
                if($u == null) continue;
                Permission::where('community_id', $s->community->id)->where('user_id', $u->id)->delete();
                $u->syncPerms($perms[$g][$m], $s->community->id);
                /*$f = DB::delete("DELETE FROM `user_va` WHERE `user_id`=? AND `cm_id`=? ", [$u->id, $s->community->id]);
                foreach($perms[$g][$m] as $a) {
                    $ds = $s->community->discordSync()->where('role_id', $a)->get();
                    foreach($ds as $d) {
                        if($d->permission != null) Permission::create(['user_id' => $u->id, 'permission' => $d->permission, 'community_id' => $s->community_id]);
                        if($d->va_id != null) DB::insert("INSERT INTO `user_va` (`user_id`, `va_id`, `cm_id`) VALUES(?, ?, ?)", [$u->id, $d->va_id, $s->community->id]);
                    }
                }*/
            }
        }
    }
}
