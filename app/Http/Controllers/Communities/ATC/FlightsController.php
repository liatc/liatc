<?php

namespace App\Http\Controllers\Communities\ATC;

use App\Events\HandoffAccept;
use App\Events\HandoffCancel;
use App\Events\HandoffRequest;
use App\Flight;
use App\Http\Controllers\Controller;
use App\Logs\FaaLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FlightsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return community()->flights;
    }

    public function search(Request $request) {
        $v = Validator::make($request->all(), [
            'query' => 'required|string|max:10'
        ]);
        if($v->fails()) return response(['errors' => $v->errors()], 400);
        $flights = community()->flights()->where('callsign', $request->get('query'))->withTrashed()->orderBy('id', 'desc')->limit(10)->get();
        return ['flights' => $flights];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'callsign' => 'required|string|max:15',
            'type' => 'required|string|max:4'
        ]);
        if($v->fails()) return response(['errors' => $v->errors()], 400);
        $ap = explode('_', Auth::user()->station_name)[0];
        $f = Flight::create(['ifr' => false, 'callsign' => $request->get('callsign'), 'aircraft_type' => $request->get('type'),
            'true_airspeed' => 0, 'departure_time' => Carbon::now(), 'altitude' => 3000,
            'routing' => 'VFR', 'remarks' => '/VFRGEN/' . Auth::user()->station_name . '/', 'current_station' => Auth::user()->station_name,
            'pilot_id' => 0, 'origin' => 'K' . $ap, 'destination' => 'K' . $ap, 'beacon_code' => 1200, 'community_id' => community()->id,
            'active' => true]);
        //FaaLog::Entry(FaaLog::CREATE_FLIGHT, "Callsign: " . $f->callsign . "\n\nAircraft Type: " . $f->type . "\n\n[VFR]", $f->id);
        return $f;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function show(Flight $flight)
    {
        if($flight->community_id != community()->id) return abort(403);
        return $flight;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight)
    {
        if($flight->community_id != community()->id) return abort(403);
        $v = Validator::make($request->all(), [
            'type' => 'nullable|string|max:4',
            'requested_alt' => 'nullable|integer|min:1|max:50000',
            'assigned_alt' => 'nullable|integer|min:1|max:50000',
            'route' => 'nullable|string',
            'beacon_code' => 'nullable|integer|min:1|max:9999',
            'current_station' => 'nullable|string|max:15'
        ]);
        if($v->fails()) return response(['errors' => $v->errors(), 'flight' => $flight], 400);
        if($request->has('aircraft_type')) {
            $flight->aircraft_type = $request->get('aircraft_type');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set type to " . $flight->type, $flight->id);
        }
        if($request->has('altitude')) {
            $flight->altitude = $request->get('altitude');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set requested alt to " . $flight->requested_alt, $flight->id);
        }
        if($request->has('assigned_altitude')) {
            $flight->assigned_altitude = $request->get('assigned_altitude');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set assigned alt to " . $flight->assigned_alt, $flight->id);
        }
        if($request->has('routing')) {
            $flight->routing = $request->get('routing');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set route to:\n\n" . $flight->route, $flight->id);
        }
        if($request->has('beacon_code')) {
            $flight->beacon_code = $request->get('beacon_code');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set beacon code to " . $flight->beacon_code, $flight->id);
        }
        if($request->has('notes')) {
            $flight->notes = $request->get('notes');
            //FaaLog::Entry(FaaLog::MODIFY_FLIGHT, "Set notes to:\n\n" . $flight->comments, $flight->id);
        }
        $flight->save();
        return response($flight, 200);
    }

    public function claim(Flight $flight) {
        $flight->current_station = Auth::user()->station_name;
        $flight->save();
        //$log = FaaLog::Entry(FaaLog::CLAIM_FLIGHT, null, $flight->id);
        //$log->flag = true;
        //$log->save();
        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        if($flight->community_id != community()->id) return abort(403);
        $flight->delete();
        //FaaLog::Entry(FaaLog::ARCHIVE_FLIGHT, null, $flight->id);
        return response(null, 204);
    }

    public function restore($flight) {
        $f = Flight::where('id', $flight)->onlyTrashed()->first();
        if($f == null) abort(404);
        if($f->community_id != community()->id) return abort(403);
        $f->restore();
        $f->pending_handoff = null;
        $f->current_station = Auth::user()->station_name;
        $f->save();
        //FaaLog::Entry(FaaLog::RESTORE_FLIGHT, null, $f->id);
        return response(null, 204);
    }

    public function handoff(Request $request, Flight $flight) {
        if($flight->community_id != community()->id) return abort(403);
        $v = Validator::make($request->all(), [
            'new_station' => 'required|string|max:15'
        ]);
        if($v->fails()) return response(['errors' => $v->errors()], 400);
        $flight->pending_handoff = strtoupper($request->get('new_station'));
        $flight->save();
        event(new HandoffRequest($flight));
        //FaaLog::Entry(FaaLog::HANDOFF_INITIATE, "Target Station: " . $flight->pending_handoff, $flight->id);
        return response(null, 204);
    }

    public function cancelHandoff(Flight $flight) {
        if($flight->community_id != community()->id) return abort(403);
        $flight->pending_handoff = null;
        $flight->save();
        event(new HandoffCancel($flight));
        //FaaLog::Entry(FaaLog::HANDOFF_CANCEL, null, $flight->id);
        return response(null, 204);
    }

    public function acceptHandoff(Flight $flight) {
        if($flight->community_id != community()->id) return abort(403);
        if($flight->pending_handoff == null) return abort(400);
        $flight->current_station = $flight->pending_handoff;
        $flight->pending_handoff = null;
        $flight->save();
        event(new HandoffAccept($flight));
        //FaaLog::Entry(FaaLog::HANDOFF_ACCEPT, null, $flight->id);
        return response(null, 204);
    }
}
