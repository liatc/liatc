<?php

namespace App\Http\Controllers\Communities\ATC;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function getMessages() {
        $m = community()->messages()->orderBy('id', 'desc')->limit(5)->get();
        return ['messages' => array_reverse($m->toArray())];
    }

    public function postMessage(Request $req, $room = "controllers") {
        $v = Validator::make($req->all(), [
            'message' => 'required|string|max:200'
        ]);
        if($v->fails()) return response(['errors' => $v->errors()], 400);
        $m = Message::create(['user_id' => Auth::user()->id, 'community_id' => community()->id, 'message' => $req->get('message')]);
        return ['message' => $m];
    }
}
