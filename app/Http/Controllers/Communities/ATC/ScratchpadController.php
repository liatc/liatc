<?php

namespace App\Http\Controllers\Communities\ATC;

use App\Http\Controllers\Controller;
use App\Logs\FaaLog;
use App\Scratchpad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ScratchpadController extends Controller
{
    private static function getScratch() {
        $s = community()->scratchpad;
        if($s == null) {
            $s = Scratchpad::create(['community_id' => community()->id]);
        }
        return $s;
    }

    public function getScratchpad() {
        $s = self::getScratch();
        return $s;
    }

    public function lockScratchpad() {
        $s = self::getScratch();
        if($s->owner_id != null) return response(null, 401);
        $s->owner_id = Auth::user()->id;
        $s->save();
        //FaaLog::Entry(FaaLog::LOCK_SCRATCHPAD);
        return response(null, 204);
    }

    public function submitContents(Request $request) {
        $v = Validator::make($request->all(), [
            'contents' => 'nullable|string'
        ]);
        if($v->fails()) return response(['errors' => $v->errors()], 400);
        $s = self::getScratch();
        if($s->owner_id != Auth::user()->id) return response(null, 401);
        $s->contents = $request->get('contents');
        $s->owner_id = null;
        $s->save();
        //FaaLog::Entry(FaaLog::SAVE_SCRATCHPAD, "Set contents to:\n\n" . $s->contents);
        return response(null, 204);
    }
}
