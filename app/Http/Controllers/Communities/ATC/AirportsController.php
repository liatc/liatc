<?php

namespace App\Http\Controllers\Communities\ATC;

use App\Airport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AirportsController extends Controller
{
    public function getAirports() {
        return community()->airports;
    }

    public function getCharts(Airport $airport) {
        if($airport->community_id != community()->id) abort(403);
        return $airport->charts;
    }
}
