<?php

namespace App\Http\Controllers\Communities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ATCController extends Controller
{
    public function atcdash() {
        return view('communities.atc.home');
    }
    public function flightstrips() {
        if(Auth::user()->station_name == null) return redirect(coute('c.atc.frequency'));
        $token = Auth::user()->api_token;
        if($token == null) $token = Auth::user()->genApiToken();
        return view('communities.atc.strips', ['token' => $token]);
    }
    public function active() {
        if(Auth::user()->station_name == null) return redirect(coute('c.atc.frequency'));
        $token = Auth::user()->api_token;
        if($token == null) $token = Auth::user()->genApiToken();
        return view('communities.atc.active', ['token' => $token]);
    }

    public function changeFreq() {
        return view('communities.atc.frequency');
    }

    public static function validateStationName($name) {
        $spl = explode('_', $name);
        $sthelp = "Invalid Station Name. Station names should contain the IATA code of the airport along with the position. Example: LAX_TWR";
        $phelp = "Invalid Position. Position should be one of: DEL, TWR, DEP, APP, CTR, OCE, OBS";
        if(count($spl) < 2) return $sthelp;
        if(count($spl) > 3) return $sthelp;
        $name = $spl[0];
        $st = $spl[count($spl)-1];
        if(strlen($name) != 3 || strlen($st) != 3) return $sthelp;
        if($st !== "DEL" && $st !== "TWR" && $st !== "DEP" && $st !== "APP" && $st !== "CTR" && $st !== "OCE" && $st !== "OBS") return $phelp;
        return true;
    }

    public function processFreqChange(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'frequency' => 'required|numeric|between:118,137'
        ]);
        $v = self::validateStationName($request->get('name'));
        if($v !== true) return redirect()->back()->withErrors(['name' => $v])->withInput();
        $u = Auth::user();
        $u->station_name = $request->get('name');
        $u->frequency = $request->get('frequency');
        $u->save();
        return redirect()->back()->with('success_message', 'Frequency saved successfully.');
    }
}
