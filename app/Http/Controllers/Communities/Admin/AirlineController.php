<?php

namespace App\Http\Controllers\Communities\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\VirtualAirline;
use App\Webhook;
use Illuminate\Http\Request;

class AirlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $airlines = community()->virtualAirlines()->paginate(20);
        return view('communities.admin.airlines.index', ['airlines' => $airlines]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('communities.admin.airlines.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'icao' => 'required|string|max:4',
            'owner_id' => 'required|integer'
        ]);
        $u = User::where('discord_id', $request->get('owner_id'))->first();
        if($u == null) return redirect()->back()->withErrors(['owner_id' => 'The owner could not be found. Ensure that they have logged into LithiumATC.'])->withInput();
        if(community()->getPlan()->getMaxVA() != -1) {
            $current = community()->airlines()->count();
            if($current >= community()->getPlan()->getMaxVA()) return redirect()->back()->withErrors(['feature', 'You have reached the maximum number of virtual airlines.']);
        }
        $v = VirtualAirline::create(['name' => $request->get('name'), 'icao' => $request->get('icao'),
            'owner_id' => $u->id, 'community_id' => community()->id]);
        return redirect(coute('c.admin.airlines.edit', ['airline' => $v]))->with('success_message', 'Airline created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VirtualAirline  $virtualAirline
     * @return \Illuminate\Http\Response
     */
    public function show(VirtualAirline $virtualAirline)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VirtualAirline  $virtualAirline
     * @return \Illuminate\Http\Response
     */
    public function edit($community, VirtualAirline $airline)
    {
        return view('communities.admin.airlines.edit', ['va' => $airline]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VirtualAirline  $virtualAirline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $community, VirtualAirline $airline)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'icao' => 'required|string|max:4',
            'owner_id' => 'required|integer'
        ]);
        $u = User::where('discord_id', $request->get('owner_id'))->first();
        if($u == null) return redirect()->back()->withErrors(['owner_id' => 'The owner could not be found. Ensure that they have logged into LithiumATC.'])->withInput();
        $airline->name = $request->get('name');
        $airline->icao = $request->get('icao');
        $airline->owner_id = $u->id;
        $airline->save();
        return redirect()->back()->with('success_message', 'Airline saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VirtualAirline  $virtualAirline
     * @return \Illuminate\Http\Response
     */
    public function destroy($community, VirtualAirline $airline)
    {
        $airline->delete();
        return redirect(coute('c.admin.airlines.index'))->with('success_message', 'Airline deleted :(');
    }

    public function newHook($community, VirtualAirline $airline) {
        return view('communities.admin.airlines.webhook', ['airline' => $airline]);
    }

    public function addHook(Request $request, $community, VirtualAirline $airline) {
        $this->validate($request, [
            'url' => "required|url|regex:/https:\/\/discordapp.com\/api\/webhooks\/[0-9]{1,}\/.{1,}/"
        ]);
        Webhook::create(['community_id' => community()->id, 'url' => $request->get('url'), 'type' => Webhook::WEBHOOK_VIRTUAL_AIRLINE, 'object_id' => $airline->id]);
        return redirect(coute('c.admin.airlines.edit', ['airline' => $airline]))->with('success_message', 'Webhook added successfully');
    }

    public function delHook(Request $request, $community, VirtualAirline $airline, Webhook $hook) {
        if($hook->type != Webhook::WEBHOOK_VIRTUAL_AIRLINE || $hook->object_id != $airline->id || $airline->community_id != community()->id) abort(403);
        $hook->delete();
        return redirect(coute('c.admin.airlines.edit', ['airline' => $airline]))->with('success_message', 'Webhook deleted successfully');
    }
}
