<?php

namespace App\Http\Controllers\Communities\Admin;

use App\Community;
use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function overview() {
        $c = community();
        $uc = Permission::where('community_id', $c->id)->where('permission', 'user')->count() + 1;
        return view('communities.admin.overview', ['uc' => $uc]);
    }

    public function saveOverview(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:100'
        ]);
        $c = community();
        $c->name = $request->get('name');
        $c->save();
        return redirect()->back()->with('success_message', 'Settings saved successfully');
    }

    public function settings() {
        $s = Community::AVAILABLE_SETTINGS;
        return view('communities.admin.settings', ['a' => $s, 'current' => community()->customization]);
    }

    public function saveSettings(Request $request) {
        $v = [];
        $c = Community::AVAILABLE_SETTINGS;
        foreach(array_keys($c) as $k) {
            foreach(array_keys($c[$k]) as $f) {
                $v[$f] = $c[$k][$f]['validators'];
                if($request->has($f) && $request->get($f) != "" && $c[$k][$f]['is_color']) {
                    var_dump($f . ' = ' . $request->get('f'));
                    $x = Community::resolveColor($request->get($f), $c[$k][$f]['is_background']);
                    if($x == null) return redirect()->back()->withInput()->withErrors([$f => 'Invalid Color']);
                }
            }
        }
        $this->validate($request, $v);
        foreach(array_keys($c) as $k) {
            foreach (array_keys($c[$k]) as $f) {
                $s = $c[$k][$f];
                if(!$request->has($f)) continue;
                community()->putSetting($f, $request->get($f));
            }
        }
        return redirect()->back()->with('success_message', 'Settings saved successfully.');
    }
}
