<?php

namespace App\Http\Controllers\Communities\Admin;

use App\Community;
use App\DiscordSync;
use App\Http\Controllers\Controller;
use App\VirtualAirline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DiscordSyncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sPerms = community()->discordSync()->where('permission', '!=', null)->get();
        $va = community()->discordSync()->where('va_id', '!=', null)->get();
        return view('communities.admin.discord.index', ['perms' => $sPerms, 'va' => $va]);
    }

    public function startLink() {
        //error_log('f');
        //return 'f';
        Session::put('return_cid', community()->id);
        return redirect('https://discordapp.com/api/oauth2/authorize?response_type=code&client_id='.env('DISCORD_KEY').'&scope=bot&redirect_uri=' . urlencode(route('discord.link')));
    }

    public function link(Request $request) {
        $this->validate($request, [
            'guild_id' => 'required|integer'
        ]);
        $c = Community::find(Session::get('return_cid'));
        if($c == null) abort(404);
        $c->putSetting('discord_server_id', $request->get('guild_id'));
        return redirect()->route('c.admin.discord.index', ['community' => $c->subdomain]);
    }

    public function unlink() {
        community()->putSetting('discord_server_id', null);
        community()->permissions()->delete();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $type = $request->has('type') ? $request->get('type') : 'permission';
        return view('communities.admin.discord.edit', ['type' => $type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|integer',
            'permission' => 'nullable|string|max:20',
            'va' => 'nullable|integer'
        ]);
        if(!$request->has('permission') && !$request->has('va')) return redirect()->back()->withInput();
        if($request->has('permission')) {
            DiscordSync::create(['role_id' => $request->get('role'), 'community_id' => community()->id, 'permission' => $request->get('permission')]);
            return redirect(coute('c.admin.discord.index'))->with('success_message', 'Permission added successfully.');
        }
        if($request->has('va')) {
            if(VirtualAirline::find($request->get('va')) == null) return abort(400);
            DiscordSync::create(['role_id' => $request->get('role'), 'community_id' => community()->id, 'va_id' => $request->get('va')]);
            return redirect(coute('c.admin.discord.index'))->with('success_message', 'VA Role added successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DiscordSync  $discordSync
     * @return \Illuminate\Http\Response
     */
    public function show(DiscordSync $discordSync)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DiscordSync  $discordSync
     * @return \Illuminate\Http\Response
     */
    public function edit($community, DiscordSync $discord)
    {
        if($discord->community_id != community()->id) abort(403);
        return view('communities.admin.discord.edit', ['discord' => $discord]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DiscordSync  $discordSync
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $community, DiscordSync $discord)
    {
        if($discord->community_id != community()->id) abort(403);
        $this->validate($request, [
            'role' => 'required|integer',
            'permission' => 'nullable|string|max:20',
            'va' => 'nullable|integer'
        ]);
        if(!$request->has('permission') && !$request->has('va')) return redirect()->back()->withInput();
        if($request->has('permission')) {
            $discord->permission = $request->get('permission');
            $discord->save();
            return redirect(coute('c.admin.discord.index'))->with('success_message', 'Permission saved successfully.');
        }
        if($request->has('va')) {
            $discord->va_id = $request->get('va');
            $discord->save();
            return redirect(coute('c.admin.discord.index'))->with('success_message', 'VA Role saved successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DiscordSync  $discordSync
     * @return \Illuminate\Http\Response
     */
    public function destroy($community, DiscordSync $discord)
    {
        if($discord->community_id != community()->id) abort(403);
        $discord->delete();
        return redirect(coute('c.admin.discord.index'))->with('success_message', 'Permission deleted successfully.');
    }
}
