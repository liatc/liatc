<?php

namespace App\Http\Controllers\Communities\Admin;

use App\Airport;
use App\Chart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AirportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ap = community()->airports()->paginate(15);
        return view('communities.admin.airports.index', ['aps' => $ap]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('communities.admin.airports.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'icao' => 'required|string|max:4',
            'name' => 'required|string|max:100'
        ]);
        $a = Airport::create(['community_id' => community()->id, 'icao' => $request->get('icao'), 'name' => $request->get('name')]);
        return redirect(coute('c.admin.airports.edit', ['airport' => $a]))->with('success_message', 'Airport created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function show($community, Airport $airport)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function edit($community, Airport $airport)
    {
        if($airport->community_id != community()->id) abort(403);
        return view('communities.admin.airports.edit', ['ap' => $airport]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $community, Airport $airport)
    {
        if($airport->community_id != community()->id) abort(403);
        $this->validate($request, [
            'icao' => 'required|string|max:4',
            'name' => 'required|string|max:100'
        ]);
        $airport->icao = $request->get('icao');
        $airport->name = $request->get('name');
        $airport->save();
        return redirect()->back()->with('success_message', 'Airport modified successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function destroy($community, Airport $airport)
    {
        if($airport->community_id != community()->id) abort(403);
        $airport->delete();
        return redirect(coute('c.admin.airports.index'))->with('success_message', 'Airport Deleted Successfully');
    }

    public function newChart($community, Airport $airport) {
        if($airport->community_id != community()->id) abort(403);
        return view('communities.admin.airports.editchart', ['ap' => $airport]);
    }

    public function storeChart(Request $request, $community, Airport $airport) {
        if($airport->community_id != community()->id) abort(403);
        $this->validate($request, [
            'type' => 'required|string|max:100',
            'name' => 'required|string|max:100',
            'url' => 'required|string|url'
        ]);
        Chart::create(['airport_id' => $airport->id, 'type' => $request->get('type'), 'name' => $request->get('name'),
            'url' => $request->get('url')]);
        return redirect(coute('c.admin.airports.edit', ['airport' => $airport]))->with('success_message', 'Chart added successfully.');
    }

    public function editChart($community, Airport $airport, Chart $chart) {
        if($chart->airport->community_id != community()->id) abort(403);
        if($chart->airport_id != $airport->id) abort(404);
        return view('communities.admin.airports.editchart', ['c' => $chart]);
    }

    public function updateChart(Request $request, $community, Airport $airport, Chart $chart) {
        if($chart->airport->community_id != community()->id) abort(403);
        if($chart->airport_id != $airport->id) abort(404);
        $this->validate($request, [
            'type' => 'required|string|max:100',
            'name' => 'required|string|max:100',
            'url' => 'required|string|url'
        ]);
        $chart->type = $request->get('type');
        $chart->name = $request->get('name');
        $chart->url = $request->get('url');
        $chart->save();
        return redirect(coute('c.admin.airports.edit', ['airport' => $airport]))->with('success_message', 'Chart modified successfully.');
    }

    public function deleteChart(Request $request, $community, Airport $airport, Chart $chart) {
        if($chart->airport->community_id != community()->id) abort(403);
        if($chart->airport_id != $airport->id) abort(404);
        $chart->delete();
        return redirect(coute('c.admin.airports.edit', ['airport' => $airport]))->with('success_message', 'Chart deleted successfully.');
    }
}
