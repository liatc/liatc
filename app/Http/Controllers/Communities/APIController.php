<?php

namespace App\Http\Controllers\Communities;

use App\Flight;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getFlights() {
        return community()->flights;
    }

    public function endFlight(Flight $flight) {
        if($flight->community_id != community()->id) abort(403);
        $flight->delete();
        return response(null, 204);
    }
}
