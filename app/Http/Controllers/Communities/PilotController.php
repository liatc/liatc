<?php

namespace App\Http\Controllers\Communities;

use App\Airport;
use App\Flight;
use App\VirtualAirline;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PilotController extends Controller
{
    public function pilotdash() {
        $cf = Auth::user()->active_flight;
        $arc = Auth::user()->flights()->where('active', true)->onlyTrashed()->orderBy('id', 'desc')->limit(10)->get();
        return view('communities.pilot.home', ['cf' => $cf, 'arc' => $arc]);
    }

    public function fileplan() {
        $vas = Auth::user()->airlines;
        return view('communities.pilot.plan', ['vas' => $vas]);
    }

    public function processFile(Request $request) {
        $this->validate($request, [
            'va' => 'nullable|integer',
            'callsign' => 'required|string',
            'type' => 'required|string|max:4',
            'origin' => 'required|string|max:4',
            'destination' => 'required|string|max:4',
            'alternative' => 'nullable|string|max:4',
            'altitude' => 'required|integer|max:50000|min:1000',
            'true_airspeed' => 'required|integer|min:1|max:700',
            'route' => 'required|string|max:200',
            'remarks' => 'nullable|string|max:700',
            'after' => 'required|string'
        ]);
        $va = VirtualAirline::find($request->get('va'));
        if($va == null && $request->get('va') !== '0') return redirect()->back()->withErrors(['va' => 'VA not in database'])->withInput();
        $active = $request->get('after') == 'Submit';
        $f = Flight::where('pilot_id', Auth::user()->id)->get();
        foreach($f as $a) {
            $a->delete();
        }
        $station = strtoupper($request->get('origin'));
        if(strpos($station, 'K') === 0) $station = substr($station, 1);
        $station .= "_DEL";
        $fl = Flight::create(['airline_id' => $va != null ? $va->id : null, 'callsign' => $request->get('callsign'), 'aircraft_type' => $request->get('type'),
            'origin' => $request->get('origin'), 'destination' => $request->get('destination'), 'alternative' => $request->get('alternative'),
            'altitude' => $request->get('altitude'), 'true_airspeed' => $request->get('true_airspeed'), 'routing' => $request->get('route'),
            'remarks' => $request->get('remarks'), 'active' => $active, 'pilot_id' => Auth::user()->id, 'departure_time' => Carbon::now()->addMinutes(15),
            'community_id' => community()->id, 'current_station' => $station]);
        if($active)
            return redirect()->back()->with('message', 'Plan was filed and will be retained for two hours.');
        else
            return redirect(coute('c.fileplan.review', ['plan' => $fl->id]));
    }

    public function reviewPlan(Request $request, $community, Flight $plan) {
        $plan = Flight::where('community_id', community()->id)->where('pilot_id', Auth::user()->id)->where('active', false)->find($plan->id);
        if($plan == null) abort(404);
        return view('communities.pilot.reviewplan', ['plan' => $plan]);
    }

    public function cancelReview(Request $request, $community, Flight $plan) {
        $r = redirect(coute('c.fileplan'))->withInput([
            'va' =>  $plan->airline_id,
            'callsign' => $plan->callsign,
            'type' => $plan->aircraft_type,
            'origin' => $plan->origin,
            'destination' => $plan->destination,
            'alternative' => $plan->alternative,
            'altitude' => $plan->altitude,
            'true_airspeed' => $plan->true_airspeed,
            'route' => $plan->routing,
            'remarks' => $plan->remarks
        ]);
        $plan->forceDelete();
        return $r;
    }

    public function file(Request $request, $community, Flight $plan) {
        $f = Flight::where('pilot_id', Auth::user()->id)->where('active', true)->get();
        foreach($f as $a) {
            $a->delete();
        }
        $plan->active = true;
        $plan->save();
        return redirect(coute('c.fileplan'))->with('message', 'Plan was filed and will be retained for two hours.');
    }

    public function cancelCurrent() {
        $f = Flight::where('pilot_id', Auth::user()->id)->where('active', true)->first();
        if($f == null) return redirect()->back();
        $f->delete();
        return redirect()->back();
    }

    public function getCharts(Request $request) {
        $aps = community()->airports;
        if($request->has('airport')) {
            $ap = Airport::find($request->get('airport'));
            if($ap == null) return abort(404);
            if($ap->community_id != community()->id) abort(403);
            return view('communities.pilot.charts', ['ap' => $ap, 'aps' => $aps]);
        }
        return view('communities.pilot.charts', ['aps' => $aps]);
    }
}
