<?php

namespace App\Http\Controllers\Communities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        return view('communities.home', ['c' => community()]);
    }

    public function userlimit() {
        if(!community()->isExceedingConcurrent()) return redirect('/');
        return view('communities.maxusers', ['c' => community()]);
    }

    public function suspended() {
        if(community()->enabled) return redirect('/');
        return view('communities.suspended');
    }
}
