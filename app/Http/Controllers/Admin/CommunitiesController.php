<?php

namespace App\Http\Controllers\Admin;

use App\Community;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CommunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.communities.index', ['comms' => Community::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.communities.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'owner_id' => 'required|integer',
            'subdomain' => ['required','string','max:50',Rule::unique('communities', 'subdomain')],
            'plan' => 'required|integer|between:0,2'
        ]);
        $ow = User::where('discord_id', $request->get('owner_id'))->first();
        if($ow == null) return redirect()->back()->withErrors(['owner_id' => 'User not found in database'])->withInput();
        Community::create(['name' => $request->get('name'), 'owner_id' => $ow->id, 'subdomain' => $request->get('subdomain'), 'plan_id' => $request->get('plan')]);
        return redirect()->route('admin.communities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function show(Community $community)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function edit(Community $community)
    {
        return view('admin.communities.edit', ['community' => $community]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Community $community)
    {
        if($request->get('action') == "Deactivate") {
            $community->enabled = false;
            $community->save();
            return redirect()->back();
        }
        if($request->get('action') == "Activate") {
            $community->enabled = true;
            $community->save();
            return redirect()->back();
        }
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'owner_id' => 'required|integer',
            'subdomain' => ['required','string','max:50',Rule::unique('communities', 'subdomain')->ignore($community->id)],
            'plan' => 'required|integer|between:0,2'
        ]);
        $community->name = $request->get('name');
        $ow = User::where('discord_id', $request->get('owner_id'))->first();
        if($ow == null) return redirect()->back()->withErrors(['owner_id' => 'User not found in database'])->withInput();
        $community->owner_id = $ow->id;
        $community->subdomain = $request->get('subdomain');
        $community->plan_id = $request->get('plan');
        $community->save();
        return redirect()->route('admin.communities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Community $community)
    {
        //
    }
}
