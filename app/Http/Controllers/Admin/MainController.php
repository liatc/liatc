<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Issue;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home() {
        return view('admin.home');
    }

    public function settings() {
        $i = Issue::getIssue();
        return view('admin.settings', ['i' => $i]);
    }

    public function saveBanner(Request $request) {
        $i = Issue::getIssue();
        $this->validate($request, [
            'title' => 'required|string|max:100',
            'description' => 'required|string'
        ]);
        $i->title = $request->get('title');
        $i->description = $request->get('description');
        $i->save();
        return redirect()->route('admin.settings')->with('success_message', 'Banner saved.');
    }

    public function activateBanner() {
        $i = Issue::getIssue();
        $i->active = true;
        $i->save();
        return redirect()->route('admin.settings')->with('success_message', 'Banner now active.');
    }

    public function deactivateBanner() {
        $i = Issue::getIssue();
        $i->active = false;
        $i->save();
        return redirect()->route('admin.settings')->with('success_message', 'Banner no longer active.');
    }
}
