<?php

namespace App\Http\Middleware;

use App\ApiKey;
use Closure;

class VerifyApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $p)
    {
        $key = $request->header('authorization');
        if($key == null) abort(401);
        $k = ApiKey::where('key', hash('sha256', $key))->first();
        if($k == null) abort(403);
        if($k->purpose != $p) abort(403);
        return $next($request);
    }
}
