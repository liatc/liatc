<?php

namespace App\Http\Middleware;

use Closure;

class RequiresFeature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $f)
    {
        if(!$this->hasFeature($f)) return abort(403);
        return $next($request);
    }

    public function hasFeature($f) {
        $plan = community()->getPlan();
        switch($f) {
            case 'atc':
                return $plan->getCanUseATC();
            case 'discord':
                return $plan->getCanLinkDiscord();
            case 'customize':
                return $plan->getCanCustomize();
            case 'flight-strips':
                return $plan->getCanUseFlightStrips();
            case 'cookie':
                return $plan->hasCookie();
            default:
                return false;
        }
    }
}
