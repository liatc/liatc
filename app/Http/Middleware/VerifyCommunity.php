<?php

namespace App\Http\Middleware;

use App\Permission;
use App\VirtualAirline;
use Closure;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class VerifyCommunity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(community() == null) return redirect(env('APP_URL'));
        if(!community()->enabled && Route::getCurrentRoute()->getName() != "suspended") return redirect('/suspended');
        if(!Session::has('perms.' . community()->id) && community()->getSetting('discord_server_id') != null && Auth::check()) {
            $arr = [];
            if(env('APP_ENV') == 'local' && Auth::user()->id == community()->owner_id) {
                Auth::user()->permissions()->delete();
                foreach(Permission::AVAILABLE_PERMISSIONS as $a) {
                    Permission::create(['user_id' => Auth::user()->id, 'permission' => $a, 'community_id' => community()->id]);
                }
                DB::delete("DELETE FROM `user_va` WHERE `user_id`=? AND `cm_id`=? ", [Auth::user()->id, community()->id]);
                foreach(VirtualAirline::get() as $a) {
                    DB::insert("INSERT INTO `user_va` (`user_id`, `va_id`, `cm_id`) VALUES(?, ?, ?)", [Auth::user()->id, $a->id, community()->id]);
                }
                return $next($request);
            }
            $client = new Client();
            $r = $client->get(env('PERMISSION_QUERY') . '/query?id=' . urlencode(Auth::user()->discord_id) . '&guild=' . urlencode(community()->getSetting('discord_server_id')));
            $arr = json_decode($r->getBody());
            Auth::user()->permissions()->delete();
            $u = Auth::user();
            $u->syncPerms($arr, community()->id);
            /*DB::delete("DELETE FROM `user_va` WHERE `user_id`=? AND `cm_id`=? ", [$u->id, community()->id]);
            foreach($arr as $a) {
                $ds = community()->discordSync()->where('role_id', $a)->get();
                foreach($ds as $d) {
                    if($d->permission != null) Permission::create(['user_id' => Auth::user()->id, 'permission' => $d->permission, 'community_id' => community()->id]);
                    if($d->va_id != null) DB::insert("INSERT INTO `user_va` (`user_id`, `va_id`, `cm_id`) VALUES(?, ?, ?)", [Auth::user()->id, $d->va_id, community()->id]);
                }
            }*/
            Session::put('perms.' . community()->id, true);
        }
        return $next($request);
    }
}
