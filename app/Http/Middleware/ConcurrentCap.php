<?php

namespace App\Http\Middleware;

use App\Activity;
use App\Presence;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class ConcurrentCap
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $exc = community()->isExceedingConcurrent();
        if($exc) return redirect(coute('userlimit'));
        $a = Activity::where('user_id', Auth::user()->id)->first();
        if($a == null) {
            Activity::create(['user_id' => Auth::user()->id, 'community_id' => community()->id]);
            return $next($request);
        }
        $a->community_id = community()->id;
        $a->updated_at = Carbon::now();
        $a->save();
        return $next($request);
    }

    public function continueR($request, $next) {

    }
}
