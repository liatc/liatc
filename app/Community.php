<?php

namespace App;

use App\Plans\DemoPlan;
use App\Plans\ExtremePlan;
use App\Plans\StandardPlan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Community extends Model
{
    public const PLAN_STANDARD = 0;
    public const PLAN_EXTREME = 1;
    public const PLAN_DEMO = 2;

    public const AVAILABLE_SETTINGS = [
        'Look & Feel' => [
            'branding' => [
                'name' => 'Brand URL',
                'type' => 'text',
                'validators' => 'nullable|string|url|max:255',
                'is_color' => false,
                'is_background' => false
            ],
            'backgrounds-home' => [
                'name' => 'Homepage Background',
                'type' => 'text',
                'validators' => 'nullable|string|max:50',
                'is_color' => true,
                'is_background' => true
            ],
            'backgrounds-pilot' => [
                'name' => 'Pilot Background',
                'type' => 'text',
                'validators' => 'nullable|string|max:50',
                'is_color' => true,
                'is_background' => true
            ],
            'backgrounds-atc' => [
                'name' => 'ATC Background',
                'type' => 'text',
                'validators' => 'nullable|string|max:50',
                'is_color' => true,
                'is_background' => true
            ],
            'backgrounds-admin' => [
                'name' => 'Admin Background',
                'type' => 'text',
                'validators' => 'nullable|string|max:50',
                'is_color' => true,
                'is_background' => true
            ],
//            'backgrounds-nav' => [
//                'name' => 'Navbar Background',
//                'type' => 'text',
//                'validators' => 'nullable|string|max:50',
//                'is_color' => true,
//                'is_background' => true
//            ],
//            'foregrounds-nav' => [
//                'name' => 'Navbar Text Color',
//                'type' => 'text',
//                'validators' => 'nullable|string|max:50',
//                'is_color' => true,
//                'is_background' => false
//            ]
        ]
    ];

    protected $fillable = ['owner_id', 'name', 'plan_id', 'subdomain', 'enabled'];

    public function owner() {
        return $this->belongsTo('\App\User', 'owner_id');
    }

    public function getPlan() {
        switch($this->plan_id) {
            case self::PLAN_STANDARD:
                return new StandardPlan();
            case self::PLAN_EXTREME:
                return new ExtremePlan();
            case self::PLAN_DEMO:
                return new DemoPlan();
            default:
                return null;
        }
    }

    public function putSetting($setting, $value) {
        $s = Setting::where('setting', $setting)->where('community_id', $this->id)->first();
        if($s == null && $value == null) return;
        if($value == null) {
            $s->delete();
            return;
        }
        if($s == null) {
            $s = new Setting();
            $s->community_id = $this->id;
            $s->setting = $setting;
        }
        $s->value = $value;
        $s->save();

    }

    public function getSetting($setting, $default = null) {
        $s = Setting::where('setting', $setting)->where('community_id', $this->id)->first();
        if($s == null) return $default;
        return $s->value;
    }

    public function flights() {
        return $this->hasMany('App\Flight');
    }

    public function virtualAirlines() {
        return $this->hasMany('App\VirtualAirline');
    }

    public function controllerPresence() {
        return $this->hasMany('App\Presence', 'community_id');
    }

    public function discordSync() {
        return $this->hasMany('App\DiscordSync');
    }

    public function permissions() {
        return $this->hasMany('App\Permission');
    }

    public function scratchpad() {
        return $this->hasOne('App\Scratchpad');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }

    public function airports() {
        return $this->hasMany('App\Airport');
    }

    public function webhooks() {
        return $this->hasMany('App\Webhook');
    }

    public function isExceedingConcurrent() {
        $plan = $this->getPlan();
        if($plan->getMaxConcurrent() == -1) return false;
        $current = Activity::where('community_id', $this->id)->where('updated_at', '>', Carbon::now()->subMinutes(5))->count();
        $presence = Presence::where('community_id', $this->id)->count();
        $total = $current + $presence;
        if($total < $plan->getMaxConcurrent())
            return false;
        if(!Auth::check()) true;
        $included = Activity::where('community_id', community()->id)->where('user_id', Auth::user()->id)->where('updated_at', '>', Carbon::now()->subMinutes(5))->first();
        if($included != null) return false;
        else return true;
    }

    public function getOnlineUsers() {
        $current = Activity::where('community_id', $this->id)->where('updated_at', '>', Carbon::now()->subMinutes(5))->count();
        $presence = Presence::where('community_id', $this->id)->count();
        return $current+$presence;
    }

    public function getCustomizationAttribute() {
        return [
            'backgrounds-home' => $this->getSetting('backgrounds-home', 'gradient:home'),
            'backgrounds-pilot' => $this->getSetting('backgrounds-pilot', 'gradient:community'),
            'backgrounds-atc' => $this->getSetting('backgrounds-atc', 'gradient:community'),
            'backgrounds-admin' => $this->getSetting('backgrounds-admin', 'gradient:community'),
            'backgrounds-nav' => $this->getSetting('backgrounds-nav', 'color:363636'),
            'foregrounds-nav' => $this->getSetting('foregrounds-nav', 'color:ffffff'),
            'branding' => $this->getSetting('branding', asset('/img/conc2-light.png'))
        ];
    }

    public static function resolveColor($color, $bg = false) {
        $match = preg_match("/[a-zA-Z]{1,20}:[a-zA-Z0-9]{1,20}/", $color);
        if($match === false) return null;
        $spl = explode(":", $color);
        //return $spl;
        $type = $spl[0];
        $val = $spl[1];
        switch($type) {
            case 'gradient':
                if(!$bg) return null;
                if($val == "home") return "background: linear-gradient(235deg, #BABC4A 0%, #000000 100%), linear-gradient(235deg, #0026AC 0%, #282534 100%), linear-gradient(235deg, #00FFD1 0%, #000000 100%), radial-gradient(120% 185% at 25% -25%, #EEEEEE 0%, #EEEEEE 40%, #7971EA calc(40% + 1px), #7971EA 50%, #393E46 calc(50% + 1px), #393E46 70%, #222831 calc(70% + 1px), #222831 100%), radial-gradient(70% 140% at 90% 10%, #F5F5C6 0%, #F5F5C6 30%, #7DA87B calc(30% + 1px), #7DA87B 60%, #326765 calc(60% + 1px), #326765 80%, #27253D calc(80% + 1px), #27253D 100%);background-blend-mode: overlay, lighten, overlay, color-burn, normal;";
                if($val == "community") return "background: radial-gradient(ellipse farthest-side at 76% 77%, rgba(245, 228, 212, 0.25) 4%, rgba(255, 255, 255, 0) calc(4% + 1px)), radial-gradient(circle at 76% 40%, #fef6ec 4%, rgba(255, 255, 255, 0) 4.18%), linear-gradient(135deg, #ff0000 0%, #000036 100%), radial-gradient(ellipse at 28% 0%, #ffcfac 0%, rgba(98, 149, 144, 0.5) 100%), linear-gradient(180deg, #cd6e8a 0%, #f5eab0 69%, #d6c8a2 70%, #a2758d 100%);background-blend-mode: normal, normal, screen, overlay, normal;";
                break;
            case 'color':
                return ($bg ? 'background-color: ' : '') . "#" . $val . ($bg ? ' !important;' : '');
            default:
                return null;
        }
        return null;
    }
}
