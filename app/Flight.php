<?php

namespace App;

use App\Events\FlightCreated;
use App\Events\FlightDeleted;
use App\Events\FlightUpdated;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    use SoftDeletes;
    protected $fillable = ['airline_id', 'callsign', 'pilot_id', 'departure_time', 'aircraft_type',
        'origin', 'destination', 'alternative', 'altitude', 'true_airspeed', 'routing', 'remarks', 'active', 'community_id',
        'current_station', 'beacon_code', 'assigned_altitude', 'notes', 'ifr', 'pending_handoff'];

    protected $dispatchesEvents = [
        'created' => FlightCreated::class,
        'updated' =>FlightUpdated::class,
        'deleted' => FlightDeleted::class,
        'restored' => FlightCreated::class
    ];

    public function airline() {
        return $this->belongsTo('App\VirtualAirline', 'airline_id');
    }

    public function pilot() {
        return $this->belongsTo('App\User', 'pilot_id');
    }

    public function community() {
        return $this->belongsTo('App\Community');
    }

    protected static function boot() {
        parent::boot();
        Flight::updating(function ($m) {
            $m->revision++;
        });
        Flight::deleting(function($m) {
            if($m->airline == null) return;
            foreach(community()->webhooks()->where('type', Webhook::WEBHOOK_VIRTUAL_AIRLINE)->where('object_id', $m->airline_id)->get() as $wh) {
                $client = new Client();
                $client->post($wh->url, [
                    'json' => [
                        'embeds' => [$m->discordEmbed()]
                    ]
                ]);
            }
        });
    }

    public function discordEmbed() {
        return [
            'title' => $this->callsign,
            'type' => 'rich',
            'description' => 'Flight Logged by <@' . $this->pilot->discord_id . '>',
            'fields' => [
                [
                    'name' => 'Virtual Airline',
                    'value' => ($this->airline_id != null ? $this->airline->name : 'None'),
                    'inline' => false
                ],
                [
                    'name' => 'Aircraft Type',
                    'value' => $this->aircraft_type,
                    'inline' => false
                ],
                [
                    'name' => 'Origin',
                    'value' => $this->origin,
                    'inline' => true
                ],
                [
                    'name' => 'Destination',
                    'value' => $this->destination,
                    'inline' => true
                ],
                [
                    'name' => 'Route',
                    'value' => $this->routing,
                    'inline' => false
                ]
            ],
            'footer' => [
                'text' => 'LithiumATC'
            ]
        ];
    }
}
