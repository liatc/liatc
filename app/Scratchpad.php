<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scratchpad extends Model
{
    protected $fillable = ['contents', 'owner_id', 'community_id'];
    protected $appends = ['owner_name'];
    protected $dispatchesEvents = [
        'updated' => \App\Events\ScratchpadUpdate::class
    ];

    public function user() {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function community() {
        return $this->belongsTo('App\Community');
    }

    public function getOwnerNameAttribute() {
        if($this->owner_id == null) return null;
        $u = User::find($this->owner_id);
        return $u->station_name . "/" . $u->name;
    }
}
